""" This is an simple layout example

See [Readme](README.md)

This examples was stripped of the map, since it is a troublemaker due to geopoandas.
See [main_map.py](main_map.py) how the map is included.
"""

import os

import pandas as pd

from bokeh.models import ColumnDataSource

from bokeh_scripts.plot_settings import GeneralSettings
from bokeh_scripts.plot_table import TablePlot, TablePlotSwitchable
from bokeh_scripts.plot_filter import FilterControls
from bokeh_scripts.plot_histogram import HistogramPlot
from bokeh_scripts.plot_ranking import RankingPlot
from bokeh_scripts.plot_vs import VSPlot
from bokeh_scripts.plot_io import IOColumns
from bokeh_scripts.plot_selection import IOSelection
from bokeh_scripts.plot_control import ScriptControl

plot_settings_left = GeneralSettings(width=860)
plot_settings_right = GeneralSettings(width=1140)


####################
# Data Preparation #
####################

# Samples #
###########

# get meta(sample) data
data_meta = pd.read_csv(os.path.join(os.path.dirname(__file__), 'demo_samples.csv'))


def convert_data_formats(data: pd.DataFrame) -> None:
    """ Brute force approach to get good data types.

    Iterates through all columns of DataFrame and tries to change the format to integer.
    If this fails, it tries to change the format to float.
    If this also fails, no change is applied.

    :ToDo: find better solution
    """
    for column in data.columns:
        for index in data.index:
            try:
                data.loc[index, column] = int(data.loc[index, column])
            except ValueError:
                try:
                    data.loc[index, column] = float(data.loc[index, column])
                except ValueError:
                    # should be fine
                    continue

    return


convert_data_formats(data_meta)

# rename to fit to my template
data_meta = data_meta.rename(columns={'latitude': 'latitude_deg',
                                      'longitude': 'longitude_deg',
                                      'Temperature': 'temperature_degC'})

# list columns (value or label) used for widgets and tables
value_columns = ["latitude_deg", "longitude_deg", "value_1", "value_2", "temperature_degC"]
vco = value_columns
label_columns = ["label_1", "label_2"]
lco = label_columns

source_samples = ColumnDataSource(data_meta.fillna('NaN'))
print('{} samples with each {} values loaded.'.format(source_samples.data['index'].size, len(source_samples.data)))


def get_selected_data(label: str) -> list:
    """ easy access to (selected) samples

    :param label: column name
    :return: list of values
    """

    # Differ if there is a selection in the main source or not.
    if len(source_samples.selected.indices) > 0:
        return source_samples.data[label][source_samples.selected.indices]
    else:
        return source_samples.data[label]


# Stations #
############
data_stations = pd.read_csv(os.path.dirname(__file__) + '/demo_stations.csv')
data_stations = data_stations.rename(columns={'latitude': 'latitude_deg',
                                              'longitude': 'longitude_deg'})
source_stations = ColumnDataSource(data_stations.fillna('NaN'))
print('{} samples with each {} values loaded.'.format(source_stations.data['index'].size, len(source_stations.data)))


# Special Data #
################
imported_data = pd.DataFrame()


def reload_data():
    """
    Replace source_samples with combination of original data and imported columns
    :return:
    """

    # create combined data table
    source_samples.data = data_meta.join(imported_data, how='outer').fillna('NaN')

    # update data and table
    update_options(None, None, imported_data.columns.tolist())


#################
# Bokeh Widgets #
#################

# Table #
#########

# function to get the table columns stored in lco and vco
def get_sample_table_columns() -> list:
    """Control format of columns. Mismatch in actual Format will be **shown** as NaN"""
    # gather all columns but remove columns with special layout
    l_cos = [(column, {'formatter': str}) for column in lco]
    v_cos = [(column, {'formatter': float}) for column in vco
             if column not in ['Year', 'latitude_deg', 'longitude_deg', 'value_1']]

    # noinspection PyTypeChecker
    return [('Year', 'year'), ('latitude_deg', 'latitude_deg'), ('longitude_deg', 'longitude_deg'),
            ('value_1', {'title': 'val1 noFormat'}), ('value_1', {'title': 'val1 int', 'formatter': int})
            ] + l_cos + v_cos


p_tab_samples = TablePlot(source=source_samples, get_columns=get_sample_table_columns,
                          title='<b>Sample Information</b>', height=200,
                          plot_settings=plot_settings_left)


# function to get the station columns from the station source
def get_station_table_columns():
    return source_stations.column_names


# Widget to Filter the table
p_tab_filter = FilterControls(source=source_stations, width=640, plot_settings=plot_settings_left)

# Table creation like before, but switchable and source is provided by **p_tab_filter.source_n_view
#   "**p_tab_filter.source_n_view" is translated to "source=X, view=Y," with X and Y by p_tab_filter
p_tab_stations = TablePlotSwitchable(**p_tab_filter.source_n_view, get_columns=get_station_table_columns,
                                     title='Station Information', height=240, width=640,
                                     plot_settings=plot_settings_left)


# Histogram #
#############

p_histogram = HistogramPlot(get_selected_data,
                            options=value_columns,
                            option_default='value_1',
                            plot_settings=plot_settings_right)


# Ranking #
############

p_ranking = RankingPlot(get_selected_data,
                        options=label_columns,
                        option_default='label_2',
                        plot_settings=plot_settings_right)


# VS #
######

p_vs = VSPlot(get_selected_data,
              options_x=value_columns + label_columns,
              options_y=value_columns + label_columns,
              options_size=value_columns + label_columns,
              options_color=label_columns + value_columns,
              option_default_x='longitude_deg', option_default_y='temperature_degC',
              option_default_size='value_1', option_default_color='value_2',
              plot_settings=plot_settings_right)


# Input / Output #
##################

def add_frame(frame: pd.DataFrame):
    global imported_data
    imported_data = imported_data.join(frame, how='outer', rsuffix='_new')


p_iof = IOColumns(source_samples, add_frame, plot_settings=plot_settings_left)

p_ios = IOSelection(source_samples, plot_settings=plot_settings_left)


# Interface Control #
#####################

def execute_python(python_line: str):
    """" Allow execution of arbitrary code.

    Warning: This is a security risk
    """
    exec(python_line)


p_control = ScriptControl(execute_python, plot_settings=plot_settings_left)

################
# Bokeh Server #
################


# updates #
###########

def update_selection_cb(_attr, _old, _new) -> None:
    p_histogram.update()
    p_ranking.update()
    p_vs.update()
    return


source_samples.selected.on_change('indices', update_selection_cb)
source_samples.on_change('data', update_selection_cb)


def update_options(additional_value_columns: list = None,
                   additional_label_columns: list = None,
                   additional_unknown_columns=None) -> None:
    """
    Update Columns for Tables and Options for all Widgets (Histogram, Ranking an VS-Plot)
    :param additional_value_columns: Columns with contain values
    :param additional_label_columns: Columns with contain strings
    :param additional_unknown_columns: Columns which should be treated as both
    :return:
    """
    global lco, vco

    if additional_value_columns is None:
        additional_value_columns = []
    if additional_label_columns is None:
        additional_label_columns = []
    if additional_unknown_columns is None:
        additional_unknown_columns = []

    vco = value_columns + additional_value_columns + additional_unknown_columns
    lco = label_columns + additional_label_columns + additional_unknown_columns

    p_tab_samples.update_table()

    p_histogram.select_content.options = vco
    p_ranking.select_content.options = ["SampleNo", 'id'] + lco

    p_vs.select_x.options = vco + lco
    p_vs.select_y.options = vco + lco
    p_vs.select_size.options = ['None'] + vco + lco
    p_vs.select_color.options = ['None'] + lco + vco

    return


def visualize_column(column: str, plots: list, silent=False):
    """
    Easy Access to select Views in the different plots.
    :param column: name of column to visualize
    :param plots: plot/widget to address
    :param silent: option to disable Errors
    :return:
    """
    if column not in source_samples.data:
        raise Exception('Column "{}" is not in the data table'.format(column))

    if 'ranking' in plots:
        p_ranking.select_content.value = column
        plots.remove('ranking')
    if 'vs_x' in plots:
        p_vs.select_x.value = column
        plots.remove('vs_x')
    if 'vs_y' in plots:
        p_vs.select_y.value = column
        plots.remove('vs_y')
    if 'vs_color' in plots:
        p_vs.select_color.value = column
        plots.remove('vs_color')

    if not silent and len(plots) > 0:
        raise Exception("Unknown Selection: {}", plots)


# Serve #
#########

bokeh_layout = [
    [[
        p_ios.layout,
        p_tab_samples.layout,
        p_iof.layout,
        p_tab_stations.layout, p_tab_filter.layout,
        p_control.layout
    ], [p_histogram.layout,
        p_ranking.layout,
        p_vs.layout
        ]]
]

# the serving will be done in the "main_xxx.py" scripts
