import sys
import os
import random

from bokeh.plotting import curdoc
from bokeh.layouts import layout

# make the script able to work stand alone
sys.path.append(os.path.abspath(''))

from bokeh_scripts.plot_vs import VSPlot


# Data #
########

# create function to handle data
def get_values_fun(_data_label: str) -> list:
    """ Return data for plot (random output for example) """

    return [random.random()**2 for _ in range(100)]


# Content #
###########

# create VS Plot
p_vs = VSPlot(get_values_fun,
              options_x=['Option X1', 'Option X2', '...'],
              options_y=['Option Y1', 'Option Y2', '...'],
              options_size=['Option Size1', 'Option Size2', '...'],
              options_color=['Option Color1', 'Option Color2', '...'])


# Visualization #
#################

# Create the Layout
your_layout = [p_vs.layout]

# Create document/server
doc = curdoc()
doc.title = "Bokeh Example with Source"

doc.add_root(layout(your_layout))

# if bokeh is installed for command line
# run "bokeh serve examples/example_plot_vs.py" from top level
