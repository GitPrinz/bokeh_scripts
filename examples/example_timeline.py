""" Create a timeline of project

ToDo: This is untested and probably needs tuning.

"""

import pandas as pd

from bokeh_scripts.plot_timeline import TimelinePlot
from bokeh.plotting import output_file, save

timeline_data = pd.read_csv('demo_timeline.csv').rename(
    columns={'Bezeichnung': 'label',
             'Tag': 'group',
             'Von': 'start',
             'Bis': 'stop'})

# todo: this could maybe be done inside
timeline_data['start'] = pd.to_datetime(timeline_data['start'], format='%d.%m.%Y')
timeline_data['stop'] = pd.to_datetime(timeline_data['stop'], format='%d.%m.%Y')

# todo: "width=None" should work
timeline_plot = TimelinePlot(time_data=timeline_data, width=1900, label_orientation=60)

# todo: this should exist inside as a function
output_file("demo_timeline.html")
save(timeline_plot.fig)
