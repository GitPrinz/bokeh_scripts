""" This Script shows the simple layout without map

It was created to provide an alternative for installations where loading geopandas is not possible.
"""

from bokeh.plotting import curdoc
from bokeh.layouts import layout

import example_interface_simple

# Serve #
#########

doc = curdoc()
doc.title = "Sample Overview without Map"

doc.add_root(layout(example_interface_simple.bokeh_layout))
