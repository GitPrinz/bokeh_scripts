""" Basic Bokeh concepts

Partly based on <https://github.com/bokeh/bokeh-notebooks/blob/main/tutorial/01%20-%20Basic%20Plotting.ipynb>
"""

# Load modules
from bokeh.plotting import figure, curdoc
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Button
from bokeh.layouts import layout


# Data #
########

# Create a Data Source
your_source = ColumnDataSource({"x": [1, 2, 3, 4, 5], "y": [6, 7, 2, 4, 5]})


# Figure and Content #
######################

# Create a figure
your_figure = figure(plot_width=400,
                     plot_height=400)

# Create a Button
your_button = Button(label='Do Something')


# Callback function
def your_button_cb(_event):
    print('Something was done.')


# Listener to Button
your_button.on_click(your_button_cb)


# Add content to Figure
your_figure.circle(x="x", y="y", source=your_source,
                   size=15, line_color="navy",
                   fill_color="orange", fill_alpha=0.5)


# Visualization #
#################

# Create the Layout
your_layout = [your_figure, your_button]

# Create document/server
doc = curdoc()
doc.title = "Bokeh Example with Source"

doc.add_root(layout(your_layout))

# if bokeh is installed for command line
# run "bokeh serve examples/example_bokeh_source.py" from top level
