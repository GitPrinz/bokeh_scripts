""" Basic Bokeh concepts

Partly based on <https://github.com/bokeh/bokeh-notebooks/blob/main/tutorial/01%20-%20Basic%20Plotting.ipynb>
"""

# Load modules
from bokeh.plotting import figure
from bokeh.io import show

# Create a figure
your_figure = figure(plot_width=400,
                     plot_height=400)

# Add content
your_figure.circle([1, 2, 3, 4, 5], [6, 7, 2, 4, 5],
                   size=15, line_color="navy",
                   fill_color="orange", fill_alpha=0.5)

# Visualize the figure
show(your_figure)

