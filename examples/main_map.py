""" This Script adds the map to the simple layout

It was created to provide an alternative for installations where loading geopandas is not possible
"""

from bokeh.plotting import curdoc
from bokeh.layouts import layout

import example_interface_simple

from bokeh_scripts.plot_map import MapPlot

# Map #
#######

p_map = MapPlot(example_interface_simple.source_samples,
                example_interface_simple.source_stations,
                plot_settings=example_interface_simple.plot_settings_left)
p_map.map.hover.tooltips.extend([("Year", " @Year")])
p_map.add_lasso()
p_map.add_colorbar('temperature_degC', field_stations=False)
p_map.add_lines('label_2', 'id')

# Serve #
#########

doc = curdoc()
doc.title = "Sample Overview with Map"

bokeh_layout = example_interface_simple.bokeh_layout
bokeh_layout[0][0] = [p_map.map] + bokeh_layout[0][0]

doc.add_root(layout(bokeh_layout))
