============
Example Code
============

For now it is not really helpful like this. Check out the file directly in "examples" folder.

:todo: Improve example Documentation!?

------------------------
example_interface_simple
------------------------

.. automodule:: examples.example_interface_simple
    :members:
    :undoc-members:

------------------------
example_plot_vs
------------------------

.. automodule:: examples.example_plot_vs
    :members:
    :undoc-members:

------------------------
example_bokeh_basic
------------------------

.. automodule:: examples.example_bokeh_basic
    :members:
    :undoc-members:

------------------------
example_bokeh_source
------------------------

.. automodule:: examples.example_bokeh_source
    :members:
    :undoc-members:
