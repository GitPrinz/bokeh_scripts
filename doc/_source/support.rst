===============
Supporting Code
===============

--------
Plotting
--------
.. automodule:: bokeh_scripts.plotting
    :members:
    :undoc-members:

------
Errors
------
.. autoclass:: bokeh_scripts.errors.DataError
    :members:
    :undoc-members:
    :show-inheritance:
