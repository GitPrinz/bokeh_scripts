.. Python Playground documentation master file, created by
   sphinx-quickstart on Fri Aug 21 16:07:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bokeh Scripts API Documentation
===============================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   example
   interfaces
   support


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
