==================
Interfaces Classes
==================

++++++++++
Interfaces
++++++++++

-----
Table
-----
.. autoclass:: bokeh_scripts.plot_table.TablePlot
    :members:
    :undoc-members:
    :show-inheritance:

-------------------
TablePlotSwitchable
-------------------
.. autoclass:: bokeh_scripts.plot_table.TablePlotSwitchable
    :members:
    :undoc-members:
    :show-inheritance:

---
Map
---
.. autoclass:: bokeh_scripts.plot_map.MapPlot
    :members:
    :undoc-members:
    :show-inheritance:

.................
Support Functions
.................
.. automodule:: bokeh_scripts.plot_map
    :no-members: MapPlot
    :undoc-members:

---------
Histogram
---------
.. autoclass:: bokeh_scripts.plot_histogram.HistogramPlot
    :members:
    :undoc-members:
    :show-inheritance:

-------
Ranking
-------
.. autoclass:: bokeh_scripts.plot_ranking.RankingPlot
    :members:
    :undoc-members:
    :show-inheritance:

-------------------
X-/Y-Plot (VS Plot)
-------------------
.. autofunction:: bokeh_scripts.plot_vs.string2number
.. autofunction:: bokeh_scripts.plot_vs.get_range

......
VSPlot
......
.. autoclass:: bokeh_scripts.plot_vs.VSPlot
    :members:
    :undoc-members:
    :show-inheritance:

++++++++
Controls
++++++++

----------------------
Import/Export of  Data
----------------------
.. autofunction:: bokeh_scripts.plot_io.update_option_value

.........
IOColumns
.........
.. autoclass:: bokeh_scripts.plot_io.IOColumns
    :members:
    :undoc-members:
    :show-inheritance:

.............
ImportColumns
.............
.. autoclass:: bokeh_scripts.plot_io.ImportColumns
    :members:
    :undoc-members:
    :show-inheritance:

.............
ExportColumns
.............
.. autoclass:: bokeh_scripts.plot_io.ExportColumns
    :members:
    :undoc-members:
    :show-inheritance:

-------------------------------
Store/Load Selection of samples
-------------------------------
.. autofunction:: bokeh_scripts.plot_selection.restore_selection

...........
IOSelection
...........
.. autoclass:: bokeh_scripts.plot_selection.IOSelection
    :members:
    :undoc-members:
    :show-inheritance:


++++++++++++
Base Classes
++++++++++++

-------------------------
Base Class for Interfaces
-------------------------
.. autoclass:: bokeh_scripts.plot_settings.GeneralSettings
    :members:
    :undoc-members:
    :show-inheritance:

---------------------------------
Half sized Interfaces
---------------------------------
.. autoclass:: bokeh_scripts.plot_settings.HalfColumns
    :members:
    :undoc-members:
    :show-inheritance:

-------------------------------------------------
Interface with main part and smaller control part
-------------------------------------------------
.. autoclass:: bokeh_scripts.plot_settings.FigureAndControls
    :members:
    :undoc-members:
    :show-inheritance:
