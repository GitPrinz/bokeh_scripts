# Setup

Tips for stand-alone operation to understand the concept.

Overview:

- [main.py](examples/main.py): Script to demonstrate the functionality
- [plotting.py](bokeh_scripts/plotting.py): Support Functions
- [errors.py](bokeh_scripts/errors.py): Exceptions
- plot_xxx.py: Classes to create Interfaces.
    - [plot_table.py](bokeh_scripts/plot_table.py): Table.
    - [plot_map.py](bokeh_scripts/plot_map.py): Map.
    - [plot_histogram.py](bokeh_scripts/plot_histogram.py): Histogram.
    - [plot_ranking.py](bokeh_scripts/plot_ranking.py): Ranking.
    - [plot_vs.py](bokeh_scripts/plot_vs.py): X-/Y-Plot with, size and color.
    - [plot_io.py](bokeh_scripts/plot_io.py): Input/Output of Data.
    - [plot_selection.py](bokeh_scripts/plot_selection.py): Store/Load Selection of samples.
    - [plot_settings.py](bokeh_scripts/plot_settings.py): Basic Class to handle Interface Settings like width and height.
    - [plot_timepline.py](bokeh_scripts/plot_timepline.py): Create a timeline
    - [plot_timepline_ranking.py](bokeh_scripts/plot_timepline_ranking.py): Create multiple rankings in a timeline
- There are also some [examples](examples) on how to use Bokeh and this code.

Please use `make doc` to create the API Documentation.

## Terminal

`pip3 install -r requirements.txt`

Sometimes life is tricky. I had to uninstall bokeh (`pip3 uninstall bokeh`) to make it working.
I don't know why yet and expect some shadowing with another installation on my system.
If someone has an idea: I would be glad to know.

## Anaconda (for windows)

- Create a new Environment
- Install geopandas and maybe other requirements

## PyCharm

- (Select Anaconda Environment as Interpreter)
- Install requirements
- Create Run/Debug Configurations
  - Module name (instead of Script Path): `bokeh`
  - Parameters: `serve <your bokeh script or folder>` 
    e.g. `serve examples` in the top level of this repository
  - Python interpreter: make sure you have the right one
  - Working directory: your bokeh_scripts folder (or what you like)

### Lessons Learned

* Make sure your Run/Debug Configurations are actually the one you want.  
    You can use a different interpreter there by Mistake.