# Readme

These functions are the core of my data visualization of data with latitude and longitude.
I tried to separate them from the specific code since this are easy-to-implement blocks.

I created a demo file based on my own interfaces to demonstrate a possibility:
![ExampleInterface](ExampleInterface.png)

**Disclaimer:**

This is all very rough right now and likely to be change in the future (this means also the APIs). Look into the status of the other branches - I plan to merge soon... at least I should ...

There are some bugs, too, for sure.
Feel free to implement it as submodule in your Repository, but keep in mind pulling my ongoing changes can drastically change the implementation of the code.
I will try to prevent it, but in first case the code has to fit my needs!

## Other Tools

This Repository has not the idea of creating a new GUI for Data Visualization.
Please use for example [ODV](https://odv.awi.de/) if you search for a more professional tool.
I shared this code to give an easy examples for your own implementation.

Also have a look at

- [Digital Earth Viewer](https://digitalearth-hgf.de/)
- [Marine-Data.de](https://marine-data.de/)

## Participation

I am happy to receive your feedback.
PN me or use the Report Function in Git.
I will probably fix bugs, since I expect to use these scripts in future projects, but I usually will not have the time implement specific features.

## Application

If the [requirements](requirements.txt) are fulfilled you should be able to create an exemplary bokeh server from above top level by:  
- `make demo_map`
- `make demo_simple` to avoid loading of geopandas
- `make demo_timeline` to load `demo_timeline.csv` and create `demo_timeline.html`
- `python -m bokeh serve examples`
- `bokeh serve examples` 

Read the [Setup Information](SETUP.md) and use `make doc` to create API Documentation to dive in.

### Known Problems

See also [Issues](https://gitlab.com/GitPrinz/bokeh_scripts/-/issues)

#### Geopandas

The package "geopandas" used to draw the ap is quite challenging to install outside of linux.
I managed it with anaconda and pycharm on windows ([See Setup](SETUP.md)).
For mac I know of problems, but not of success atm.

## Changelog

There are other Branches!

### Version 0.5

0.5.0

- Add timeline plots

### Version 0.4

0.4.0

- Removed `download.py` containing `get_countries()` and `get_countries_without()`
- Removed need of `geopandas` for `plot_map`
- Added world map as geojson data and implemented this in `plot_map`

### Version 0.3

0.3.1

- Available as Package

## Code by / Contact

[Martin Prinzler](mailto:Martin.Pinzler@uni-bremen.de)