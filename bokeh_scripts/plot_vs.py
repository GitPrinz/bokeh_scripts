import numpy as np
import pandas as pd

from typing import Callable
from itertools import zip_longest

from bokeh.models import ColumnDataSource, ColorBar
from bokeh.models.mappers import LinearColorMapper
from bokeh.models.widgets import Select, RangeSlider, Toggle, Slider
from bokeh.plotting import figure

from bokeh_scripts.plotting import get_mapper, get_values, categorical_maps, continuous_maps
from bokeh_scripts.plot_settings import GeneralSettings, FigureAndControls
from bokeh_scripts.plot_save import SaveControls


def string2number(string: str):
    """ Convert string to number or throw ValueError"""
    try:
        return float(string)
    except ValueError:
        return np.NaN


def get_range(values: list) -> float:
    """ ignoring string and NaN in values"""
    if len(get_values(values)) > 0:
        return np.nanmax(get_values(values)) - np.nanmin(get_values(values))
    else:
        return 1


class VSPlot(FigureAndControls):

    source = ColumnDataSource({'id': [], 'x': [], 'y': [], 'size_value': [], 'size': [], 'color': [], 'legend': []})

    # create figure
    title = 'Sample Comparison'
    fig = figure(title=title, plot_height=768, plot_width=1024,
                 x_axis_label='Value', y_axis_label='Value',
                 tools="pan,box_zoom,wheel_zoom,save,reset,hover",
                 toolbar_location="right", active_scroll="wheel_zoom",
                 tooltips=[("Sample id", " @id"),
                           ("Value x", " @x"),
                           ("Value y", " @y"),
                           ("Size", " @size_value"),
                           ("Label", " @legend")]
                 )
    # output_backend="webgl" brings speed but problems

    # add glyphs to show values
    _mapper = LinearColorMapper(palette=['blue'])
    _scatter = fig.scatter(x='x', y='y', radius='size', legend='legend',
                           fill_color={"field": "color", "transform": _mapper}, source=source,
                           fill_alpha=0.5, line_color=None)
    _colorbar = ColorBar(color_mapper=_mapper, location=(0, 0), title='Colorbar')
    fig.add_layout(_colorbar, "right")
    _colorbar.title_standoff = 15

    # add setting widgets
    select_x = Select(title="Horizontal Axis (x)")
    select_y = Select(title="Vertical Axis (y)")

    select_size = Select(title="Size Axis")
    slider_size_scale = RangeSlider(title='Size Range', start=0, end=1, value=[0, 1], step=.1)
    toggle_size_auto = Toggle(label='Autoscale', active=True)

    select_color = Select(title="Color Axis")
    select_mapper = Select(title="Color Mapper", value='Linear', options=['Categorical', 'Linear', 'Log'])
    # Load Linear first seems necessary. Can not even be changed after a complete load instantly.
    #   Otherwise the plot will stop updating
    select_palette = Select(title="Color Palette", value='default', options=continuous_maps)
    toggle_x_revert = Toggle(label='Revert X Axis', active=False)
    toggle_y_revert = Toggle(label='Revert Y Axis', active=False)
    toggle_color_revert = Toggle(label='Revert Color Axis', active=False)
    toggle_color_NaN = Toggle(label='Show NaN colored Entries', active=True)

    slider_transparency = Slider(title='Transparency', start=0, end=1, value=0.5, step=.1)

    save_controls = SaveControls(fig)

    def __init__(self, get_values_fun: Callable[[str], list],
                 options_x: list, options_y: list, options_size: list = None, options_color: list = None,
                 option_default_x: str = None, option_default_y: str = None,
                 option_default_size: str = None, option_default_color: str = None,
                 width: int = None, width_controls: int = None, height: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()
                 ):
        """ Create a VS-Plot with Select-Widgets to choose from different options.

        "get_data_fun(selected_option)" will be called to collect the data.

        :param get_values_fun: function to handle dynamical data (will be called with label and returns list of data)
        :param options_x: possible labels to select_content for x axis
        :param options_y: possible labels to select_content for y axis
        :param options_size: possible labels to select_content for scaling
        :param options_color: possible labels to select_content for coloring
        :param option_default_x: initial label for x axis
        :param option_default_y: initial label for y axis
        :param option_default_size: initial label for scaling
        :param option_default_color: initial label for coloring
        """

        # deal with sizes (updates everything in second layout column
        #   last item is list and has to be skipped
        super().__init__(plot_main=self.fig, plot_controls=self.layout[1][:-1],
                         width=width, width_controls=width_controls, height=height, ratio_wh=1.1,
                         plot_settings=plot_settings)
        self.save_controls.plot_settings = plot_settings  # this does nothing ... some kind of link would be necessary
        self.set_width_controls(self.save_controls.layout[0])  # this sets all the widths again.

        self.get_values_fun = get_values_fun

        if options_size is None:
            options_size = []
        if options_color is None:
            options_color = []

        # initialize widgets
        self.select_x.options = options_x
        self.select_x.value = options_x[0] if option_default_x is None else option_default_x
        self.select_y.options = options_y
        self.select_y.value = options_y[0] if option_default_y is None else option_default_y

        self.select_size.options = ['None'] + options_size
        self.select_size.value = self.select_size.options[0] if option_default_size is None else option_default_size
        self.select_color.options = ['None'] + options_color
        self.select_color.value = self.select_color.options[0] if option_default_color is None else option_default_color

        # update size column
        def select_size_scale_cb(_attr, _old, new):
            minimum = np.nanmin(get_values(self.source.data['size_value']))
            maximum = np.nanmax(get_values(self.source.data['size_value']))
            if maximum == minimum:
                if minimum == 0:
                    maximum = 1
                else:
                    minimum = maximum / 2
            scale = (new[1] - new[0]) / (maximum - minimum)

            self.source.data['size'] = [value if np.isnan(value) else (value - minimum) * scale + new[0]
                                        for value in get_values(self.source.data['size_value'], return_nan='NaN')]
            return

        self.slider_size_scale.on_change('value', select_size_scale_cb)

        # trigger automatic scaling
        def select_size_auto_cb(_attr, _old, new):
            if new:
                range_value = get_range(self.source.data['x'])
                max_value = np.nanmax(get_values(self.source.data['size_value']))
                median_value = np.nanmedian(get_values(self.source.data['size_value']))

                if median_value == 0:
                    median_value = max_value

                # to care for extreme values ... but how?
                over_scale = max_value / median_value / 2

                self.slider_size_scale.value = [range_value / 500, range_value / 50 * over_scale]
            return

        self.toggle_size_auto.on_change('active', select_size_auto_cb)

        # update plot
        def select_cb(_attr, _old, _new):

            # color
            if self.select_mapper.value == 'Categorical':
                self.select_palette.options = categorical_maps
            else:
                self.select_palette.options = continuous_maps
            if self.select_palette.value not in self.select_palette.options:
                self.select_palette.value = self.select_palette.options[0]
                return  # the value change will trigger the callback again

            # update the data
            #   todo: everything is triggered everytime ... there could be some if clauses
            self._update_plot_data()

            # scaling
            self._update_size_slider()
            scale_before = self.slider_size_scale.value
            select_size_auto_cb(_attr, _old, True)
            if self.slider_size_scale.value == scale_before:
                # force update: no change in scaling, but values are overwritten
                select_size_scale_cb(None, None, self.slider_size_scale.value)

            self._update_color()
            self._update_labels()
            return

        # listener for select_cb
        self.select_x.on_change('value', select_cb)
        self.select_y.on_change('value', select_cb)
        self.select_size.on_change('value', select_cb)
        self.select_color.on_change('value', select_cb)
        self.select_mapper.on_change('value', select_cb)
        self.select_palette.on_change('value', select_cb)
        self.toggle_color_NaN.on_change('active', select_cb)
        self.slider_transparency.on_change('value', select_cb)

        # trigger inversion
        def toggle_x_revert_cb(_attr, _old, new):
            self.fig.x_range.flipped = new
            update_plot()

        def toggle_y_revert_cb(_attr, _old, new):
            self.fig.y_range.flipped = new
            update_plot()

        self.toggle_x_revert.on_change('active', toggle_x_revert_cb)
        self.toggle_y_revert.on_change('active', toggle_y_revert_cb)
        self.toggle_color_revert.on_change('active', select_cb)

        # update trigger
        #   has to be defined here to have access to select_cb
        def update_plot():
            """Update Histogram manually (e.g when dynamic data has changed)"""
            select_cb(None, None, None)

        # make it a class function
        self.update = update_plot

        # trigger first update
        update_plot()

        return

    @property
    def layout(self):
        return [self.fig, [
            self.select_x, self.toggle_x_revert, self.select_y, self.toggle_y_revert,
            self.select_size, self.slider_size_scale, self.toggle_size_auto,
            self.select_color, self.toggle_color_revert, self.select_mapper, self.select_palette, self.toggle_color_NaN,
            self.slider_transparency, self.save_controls.layout]]

    def layout_minimum(self):
        return [self.fig, [self.select_x, self.select_y, self.select_size, self.select_color]]

    def _update_size_slider(self):
        range_value = get_range(self.source.data['x'])
        self.slider_size_scale.end = range_value / 5
        self.slider_size_scale.step = range_value / 100
        return

    def _update_color(self):
        # color mapping
        vs_mapper = get_mapper(self.select_mapper.value, self.source.data['color'], self.select_palette.value)
        if self.toggle_color_revert.active:
            vs_mapper.palette = vs_mapper.palette[::-1]
        self._scatter.glyph.fill_color = {"field": "color", "transform": vs_mapper}
        self._scatter.glyph.fill_alpha = self.slider_transparency.value
        if self.select_mapper.value == 'Categorical':
            self._colorbar.visible = False
            self.fig.legend.visible = True
        else:
            self.fig.legend.visible = False
            self._colorbar.color_mapper = vs_mapper
            if self.select_palette.value in ['default', 'Turbo256']:
                # todo: fix colorbar changes.
                self._colorbar.visible = True
            else:
                self._colorbar.visible = False
                # temporary fix to not show wrong colorbars.
        # # ticks
        # # todo: this doesn't work yet
        # if self.select_mapper.value == 'Log':
        #     self._colorbar.ticker = LogTicker()
        # else:
        #     self._colorbar.ticker = BasicTicker()
        return

    def _update_labels(self):
        self.fig.xaxis.axis_label = self.select_x.value
        self.fig.yaxis.axis_label = self.select_y.value
        self._colorbar.title = self.select_color.value

        # start titles
        self.fig.title.text = "{y} vs. {x}".format(x=self.select_x.value, y=self.select_y.value)
        filename = "plotVS({x},{y}".format(x=self.select_x.value, y=self.select_y.value)

        # Add size and/or color to title
        if self.select_size.value is not None and self.select_size.value != "None":
            # size (and possible color)
            # add size
            self.fig.title.text = "{old} (Size by {size}".format(old=self.fig.title.text,
                                                                 size=self.select_size.value)
            filename += ",{size}".format(size=self.select_size.value)
            if self.select_size.value is not None and self.select_color.value != "None":
                # add color
                self.fig.title.text = "{old}, Color by {color}, ".format(
                    old=self.fig.title.text, color=self.select_color.value)
                filename += ",{color}".format(color=self.select_color.value)
            else:
                # only size (already done)
                self.fig.title.text = "{old}, ".format(
                    old=self.fig.title.text)
        elif self.select_color.value is not None and self.select_color.value != "None":
            # only color
            self.fig.title.text = "{old} (Color by {color}, ".format(
                old=self.fig.title.text, color=self.select_color.value)
            filename += ",{color}".format(color=self.select_color.value)
        else:
            # neither size nor color
            self.fig.title.text = "{old} (".format(
                old=self.fig.title.text)

        # Finish with number of entries
        #   right now, nans will already be reduced, but this shouldn't take much time, so let it be here ...
        number_entries = sum([not pd.isnull(xx) and not pd.isnull(yy) and not pd.isnull(size)
                              for xx, yy, size in zip(self.source.data['x'],
                                                      self.source.data['y'],
                                                      self.source.data['size'])])
        self.fig.title.text = "{old}{entries} entries in total)".format(
            old=self.fig.title.text, entries=number_entries)

        self.save_controls.filename = filename + ")"

        return

    def _update_plot_data(self):

        # get size values
        if self.select_size.value is None or \
                (self.select_size.value is not None and
                 (len(self.select_size.value) == 0 or self.select_size.value == 'None')):
            size_values = []
        else:
            # noinspection PyTypeChecker
            size_values = self.get_values_fun(self.select_size.value)

        # get color entries
        if self.select_color.value is None or \
                (self.select_color.value is not None and
                 (len(self.select_color.value) == 0 or self.select_color.value == 'None')):
            color_entries = []
        else:
            # noinspection PyTypeChecker
            color_entries = self.get_values_fun(self.select_color.value)
            if self.select_mapper.value == 'Categorical':
                color_entries = [str(value) for value in color_entries]
            else:
                color_entries = [value if isinstance(value, (float, int)) else string2number(value)
                                 for value in color_entries]

        # update source
        self._update_source(ids=self.get_values_fun('id'),
                            x_values=self.get_values_fun(self.select_x.value),
                            y_values=self.get_values_fun(self.select_y.value),
                            size_values=size_values,
                            color_entries=color_entries)
        return

    def _update_source(self, ids: list, x_values: list, y_values: list,
                       size_values: list = None, color_entries: list = None):

        if size_values is None:
            size_values = []
        if color_entries is None:
            color_entries = []

        # this list should have the same length, or be empty
        assert len(ids) == len(x_values)
        assert len(x_values) == len(y_values)
        assert len(y_values) == len(size_values) or len(size_values) == 0
        assert len(y_values) == len(color_entries) or len(color_entries) == 0

        # remove NaN values for x/y to reduce load
        #   if None, size_values will be filled with 1
        #       NaN size will later be changed to 0
        #   if None, color entries will be filled with 'Data'
        #       also NaN will be changed to 'NaN' or the row will be not included (depends on toggle_color_NaN)
        #   this will slow proper datasets down, make switchable?
        if len(color_entries) == 0:
            ids, x_values, y_values, size_values = zip(*[
                (idd, xv, yv, sv) for idd, xv, yv, sv
                in zip_longest(ids, x_values, y_values, size_values, fillvalue=1)
                if not pd.isnull(xv) and not pd.isnull(yv)])
            color_entries = ['Data' for _ in x_values]
        else:
            # replace NaN with "NaN" or remove sample
            ids, x_values, y_values, size_values, color_entries = zip(*[
                (idd, xv, yv, sv, ce if ce == ce else 'NaN') for idd, xv, yv, sv, ce
                in zip_longest(ids, x_values, y_values, size_values, color_entries, fillvalue=1)
                if not pd.isnull(xv) and not pd.isnull(yv) and (self.toggle_color_NaN.active or ce == ce)])
        if self.select_mapper.value == 'Linear' and len(set(color_entries)) == 1:
            # if this happens often and always freezes the interface, maybe catch this and change to categorical mapper
            print('Warning: The Plot will probably freeze, since now linear color range can be calculated!\n'
                  'Only one element was found: {}.'.format(list(set(color_entries))[0]))

        size_values = [np.nan if pd.isnull(val) else val for val in size_values]

        # update source
        self.source.data = {'id': ids, 'x': x_values, 'y': y_values,
                            'size_value': size_values, 'size': size_values,
                            'color': color_entries, 'legend': color_entries}
        return
