""" Saving png or svg images of figures """

from bokeh_scripts.plot_settings import GeneralSettings, HalfColumns

from bokeh.plotting import figure
from bokeh.io import export_svgs, export_png
from bokeh.models import CheckboxButtonGroup, TextInput


class SaveControls(HalfColumns):
    """ Selection of File Type and Name """

    def __init__(self, fig: figure, width: int = None, plot_settings: GeneralSettings = GeneralSettings()):

        # parent for width
        super().__init__(width=width, plot_settings=plot_settings)

        # store figure link
        self.figure = fig

        # create type selection and filename input
        self.type_select = CheckboxButtonGroup(labels=self.available_types,
                                               active=[],
                                               width=self.width_half)
        self.filename_input = TextInput(value="Plot", title="Save with generated Filename:",
                                        width=self.width_half)

        # create callback to trigger when type is selected
        def save_cb(_attr, _old, _new):
            self._save_figure()

        self.type_select.on_change('active', save_cb)

        return

    # Properties #
    ##############

    @property
    def layout(self) -> list:
        return [[self.filename_input, self.type_select]]

    @property
    def filename(self) -> str:
        return self.filename_input.value

    @filename.setter
    def filename(self, new_filename: str) -> None:
        self.filename_input.value = new_filename

    @property
    def available_types(self) -> list:
        """ List of available data formats. """
        return ['PNG', 'SVG', 'HTML(SVG)']

    # Methods #
    ###########

    def _save_figure(self):

        # preparation
        if len(self.type_select.active) == 0:
            # nothing selected
            return
        elif len(self.type_select.active) > 1:
            # multiple selected
            print('Warning: {} Plot types selected. Can only handle 1!'.format(len(self.type_select.active)))
            self.type_select.active = []
            return

        # plotting
        plot_type = self.available_types[self.type_select.active[0]]
        if plot_type == 'PNG':
            filename = export_png(self.figure, filename=self.filename + ".png")
        elif plot_type == 'SVG':
            self.figure.output_backend = "svg"
            filename = export_svgs(self.figure, filename=self.filename + ".svg")
            filename = filename[0]
        elif plot_type == 'HTML(SVG)':
            self.figure.output_backend = "svg"
            filename = export_svgs(self.figure, filename=self.filename + ".html")
            filename = filename[0]
        else:
            raise Exception('Unclear selection: {}'.format(self.available_types[self.type_select.active.index(1)]))

        # reset selection
        print('Saved as "{}"'.format(filename))
        self.type_select.active = []
        return

    # Convenience Methods #
    #######################

    def save_png(self):
        self.type_select.active = [self.type_select.labels.index('PNG')]

    def save_svg(self):
        self.type_select.active = [self.type_select.labels.index('SVG')]

    def save_html(self):
        self.type_select.active = [self.type_select.labels.index('HTML(SVG)')]
