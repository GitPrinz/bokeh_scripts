import pandas as pd

from typing import Callable

from bokeh.plotting import curdoc
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import MultiSelect, Button, TextInput, Div, FileInput

from bokeh_scripts.plot_settings import GeneralSettings, HalfColumns


def update_option_value(select_widget: MultiSelect, without=None):
    """ Update selection to new options (try to preserve old selection).

    :param select_widget: bokeh widget
    :param without: list of options to avoid by default
    """
    if without is None:
        without = []
    if select_widget.value is None \
            or len(set(select_widget.options) & set(select_widget.value)) == 0:
        # no match, select all
        select_widget.value = list(set(select_widget.options) - set(without))
    else:
        # select intersection of old selection and new options
        select_widget.value = list(set(select_widget.options) & set(select_widget.value))
    return


class ExportColumns(HalfColumns):

    button_export = Button(label='Export Data to CSV')
    select_columns = MultiSelect(title='Columns to Export', height=91)
    output_path = TextInput(title='Output Path', value='SelectedData.csv')

    update_callback = None

    def __init__(self, data_source: ColumnDataSource, width: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()):

        super().__init__(width=width, plot_settings=plot_settings)

        self.button_export.width = self.width_half
        self.select_columns.width = self.width_half
        self.output_path.width = self.width_half

        self.source = data_source

        def source_cb(_attr, _old, _new):
            self.update_settings()

        self.source.on_change('data', source_cb)

        def export_button_cb(_event):
            self.button_export.button_type = 'warning'
            curdoc().add_next_tick_callback(self.do_export)

        self.button_export.on_click(export_button_cb)

        self.update_settings()

    @property
    def layout(self):
        return [[Div(text='<b>Export Data</b>'),
                 [self.select_columns, [self.output_path, self.button_export]]]]

    def update_settings(self):
        self.update_callback = None
        self.button_export.button_type = 'default'

        self.select_columns.options = list(self.source.data)
        update_option_value(self.select_columns)
        return

    def do_export(self):
        if self.update_callback is not None:
            curdoc().remove_timeout_callback(self.update_callback)
            self.update_callback = None

        data = pd.DataFrame(self.source.data)[{'id'} | set(self.select_columns.value)]
        if len(self.source.selected.indices) > 0:
            data = data.iloc[self.source.selected.indices]
        data.set_index('id', drop=False if 'id' in self.select_columns.value else True, inplace=True)
        data.to_csv(self.output_path.value)

        self.button_export.button_type = 'success'
        self.update_callback = curdoc().add_timeout_callback(self.update_settings, 1500)
        return


class ImportColumns(HalfColumns):

    button_import = Button(label='ImportCSV')
    input_file = FileInput(accept='.csv, .*')
    select_columns = MultiSelect(title='Columns to Import', height=91)

    update_callback = None
    data = None

    def __init__(self, add_frame: Callable[[pd.DataFrame], None], width: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()):
        """
        :param add_frame: function to expect a Pandas DataFrame with the same index as of the core data.
        """

        super().__init__(width=width, plot_settings=plot_settings)

        self.button_import.width = self.width_half
        self.input_file.width = self.width_half
        self.select_columns.width = self.width_half

        self.add_frame = add_frame

        def input_file_cb(_attr, _old, _new):
            self.import_file()

        self.input_file.on_change('filename', input_file_cb)

        def export_button_cb(_event):
            self.button_import.button_type = 'warning'
            curdoc().add_next_tick_callback(self.add_columns)

        self.button_import.on_click(export_button_cb)

    @property
    def layout(self):
        return [[Div(text='<b>Import Data</b> (Source and CSV has to have "id" column)'),
                 [[self.input_file, self.button_import], self.select_columns]]]

    def import_file(self):
        self.update_callback = None
        self.button_import.button_type = 'default'

        self.data = pd.read_csv(self.input_file.filename)
        self.select_columns.options = list(self.data.columns)
        self.data.set_index('id', inplace=True)
        update_option_value(self.select_columns, without='id')

    def add_columns(self):
        if self.update_callback is not None:
            curdoc().remove_timeout_callback(self.update_callback)
            self.update_callback = None

        self.add_frame(self.data[self.select_columns.value])

        self.button_import.button_type = 'success'
        self.update_callback = curdoc().add_timeout_callback(self.import_file, 1500)
        return


class IOColumns:
    """ Combines Import and Export Column """

    def __init__(self, data_source: ColumnDataSource, add_frame: Callable[[pd.DataFrame], None], width: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()):

        width, _ = plot_settings.handle_width_height(width, None)

        self.import_columns = ImportColumns(
            add_frame,
            width=None if width is None else (width-plot_settings.widget_pixel_distance)//2,
            plot_settings=plot_settings)
        self.export_columns = ExportColumns(
            data_source,
            width=None if width is None else (width-plot_settings.widget_pixel_distance)//2,
            plot_settings=plot_settings)

    @property
    def layout(self):
        """ [ [Import Layout], [Export Layout] ] """
        return [[self.import_columns.layout], [self.export_columns.layout]]
