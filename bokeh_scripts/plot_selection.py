from os.path import exists as path_exists
from typing import List

import numpy as np

from bokeh_scripts.plot_settings import GeneralSettings

from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Button, TextInput, Div


def restore_selection(data_source: ColumnDataSource, selected_sample_indices: List[str]) -> None:
    """ Try to restore a selection

    :param data_source: ColumnDataSource of Bokeh
    :param selected_sample_indices: list with strings from "id" column of Source.
    """
    select_indices = []
    for index in selected_sample_indices:
        hits = np.where(data_source.data['id'] == index)
        if len(hits) > 1:
            print("Can't keep track on multiple indices. Dropped selection of {}".format(index))
        elif len(hits) < 1:
            print("Index {} is not longer present in Source.".format(index))
        else:
            select_indices.append(hits[0][0])
    data_source.selected.indices = select_indices


class IOSelection:

    stashed_sample_indices = None

    button_stash = Button(label='Stash')
    button_apply = Button(label='Apply')
    button_save = Button(label='Save')
    button_load = Button(label='Load')
    file_path = TextInput(value='This is not yet implemented')

    def __init__(self, data_source: ColumnDataSource, width: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()):

        self.plot_settings = plot_settings
        width, _ = self.plot_settings.handle_width_height(width, None)
        if width is not None:
            for obj in [self.button_stash, self.button_apply, self.button_save, self.button_load]:
                obj.width = (width-4*self.plot_settings.widget_pixel_distance)//3//2
            self.file_path.width = (width-4*self.plot_settings.widget_pixel_distance)//3

        self.data_source = data_source

        def button_stash_cb(_event):
            self.check_source()
            self.stashed_sample_indices = self.data_source.data['id'][self.data_source.selected.indices]
            self.update_status()

        def button_apply_cb(_event):
            if self.stashed_sample_indices is not None:
                self.check_source()
                restore_selection(self.data_source, self.stashed_sample_indices)

        def button_save(_event):
            self.check_source()
            # todo save with as jason file
            raise Exception('Please implement this')

        def button_load(_event):
            self.check_source()
            # todo
            raise Exception('Please implement this')

        self.button_stash.on_click(button_stash_cb)
        self.button_apply.on_click(button_apply_cb)
        self.button_save.on_click(button_save)
        self.button_load.on_click(button_load)

        self.update_status()

        return

    def check_source(self):
        assert isinstance(self.data_source, ColumnDataSource)
        if 'id' not in self.data_source.data:
            raise Exception('The Source has to have an column "id" with unique identifiers.')

    def update_status(self):
        """ update visual appearance of buttons"""
        if self.stashed_sample_indices is None or len(self.stashed_sample_indices) == 0:
            self.button_apply.button_type = 'default'
            self.button_apply.label = 'Nothing to Apply'
            self.button_stash.button_type = 'success'
            self.button_stash.label = 'Stash'
        else:
            self.button_apply.button_type = 'success'
            self.button_apply.label = 'Apply {}'.format(len(self.stashed_sample_indices))
            self.button_stash.button_type = 'warning'
            self.button_stash.label = 'Stash (Overwrite)'

        if path_exists(self.file_path.value):
            self.button_load.button_type = "success"
            self.button_save.button_type = "warning"
        else:
            self.button_load.button_type = "default"
            self.button_save.button_type = "success"
        return

    @property
    def layout(self):
        return [[Div(text='<b>Save/Load Selection</b> (Source has to have "id" column)'),
                 [self.button_stash, self.button_apply,
                  self.button_save, self.button_load,
                  self.file_path
                  ]]]
