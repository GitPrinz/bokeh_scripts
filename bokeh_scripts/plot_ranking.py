from typing import Callable

from bokeh.plotting import figure
from bokeh.models import ColumnDataSource
from bokeh.models.glyphs import Text
from bokeh.models.widgets import Select

from bokeh_scripts.plot_settings import GeneralSettings, FigureAndControls
from bokeh_scripts.plot_save import SaveControls


def count_labels(label_list: list, ranked: bool = True) -> tuple:
    """ Count labels in a list and sort by default """
    unique_labels = list(set(label_list))
    unique_counts = [label_list.count(unique_value) for unique_value in unique_labels]

    if ranked:
        unique_counts, unique_labels = zip(*sorted(zip(unique_counts, unique_labels), reverse=True))

    return unique_labels, unique_counts


class RankingPlot(FigureAndControls):

    _source = ColumnDataSource({'id': [], 'label': [], 'frequency': [], 'top': [], 'bottom': []})

    # create figure
    fig = figure(title='Ranking', x_axis_label='Frequency', y_axis_label='Label',
                 tools="pan,box_zoom,wheel_zoom,save,reset,hover",
                 toolbar_location="right", active_scroll="wheel_zoom",
                 tooltips=[("Place", " @id"),
                           ("Label", " @label"),
                           ("Frequency", " @frequency")])
    fig.y_range.flipped = True

    # add glyphs to show values
    fig.quad(top='top', bottom='bottom', left=0, right='frequency', source=_source)
    fig.add_glyph(_source, Text(x=-1, y='id', text='label',
                                text_align='right', text_baseline='middle'))

    # add setting widgets
    select_content = Select(title="Overview:")

    save_controls = SaveControls(fig)

    def __init__(self, get_values_fun: Callable[[str], list], options: list, option_default: str = None,
                 width: int = None, width_controls: int = None, height: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()):
        """ Create a Ranking with Select-Widget to choose from different options.

        "get_labels_fun(selected_option)" will be called to collect the labels to rank.

        :param get_values_fun: function to handle dynamical data (will be called with label and returns list of data)
        :param options: possible label to select_content
        :param option_default: initial label
        """

        # deal with sizes (updates everything in second layout column
        #         #   last item is list and has to be skipped
        super().__init__(plot_main=self.fig, plot_controls=self.layout[1][:-1],
                         width=width, width_controls=width_controls, height=height, ratio_wh=2.5,
                         plot_settings=plot_settings)

        self.select_content.options = options
        self.select_content.value = option_default

        # update plot
        #   has to be defined here to have access to class (can't have "self" attribute outside)
        def select_cb(_attr, _old, new):

            self._update_source(get_values_fun(new))

            self.fig.yaxis.axis_label = new
            self.fig.title.text = "Ranking of {name} ({count} entries in total)".format(
                name=new, count=sum(self._source.data['frequency']))
            self.save_controls.filename = "plotRank({name})".format(name=new)

        self.select_content.on_change('value', select_cb)

        # update trigger
        #   has to be defined here to have access to select_cb
        def update_plot():
            """Update Ranking manually (e.g when dynamic data has changed)"""
            select_cb(None, None, self.select_content.value)

        # make it a class function
        self.update = update_plot

        # trigger first update
        if option_default is not None:
            update_plot()

        return

    @property
    def layout(self):
        return [self.fig, [self.select_content, self.save_controls.layout]]

    def _update_source(self, labels: list):
        """
        Update the CDS with ranking of given labels.
        :param labels: labels to rank
        """

        # get ranked counts
        unique_labels, unique_counts = count_labels(
            [str(label) for label in labels if label == label and label is not None])  # remove NaN

        # calculate bar positions
        top = []
        bottom = []
        ids = list(range(1, len(unique_labels)+1))
        for idd in ids:
            top.append(idd - 0.4)
            bottom.append(idd + 0.4)

        self._source.data = {'id': ids, 'label': unique_labels,
                             'frequency': unique_counts,
                             'top': top, 'bottom': bottom}
        return
