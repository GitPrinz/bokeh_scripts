from bokeh_scripts.plot_settings import GeneralSettings, FullColumn

from bokeh.models import ColumnDataSource, TextInput, RadioButtonGroup, Button, CDSView, CustomJSFilter, CustomJS


class FilterControls(FullColumn):
    """ Filter by column content """

    overview_title = '<b>Applied Filters:</b>'

    def __init__(self, source: ColumnDataSource, width: int = None, plot_settings: GeneralSettings = GeneralSettings()):

        super().__init__(width=width, plot_settings=plot_settings)

        self.plot_settings = plot_settings

        self.column_input = TextInput(value='Enter column name',
                                      width=self.plot_settings.width_part(3, self.width))
        self.value_input = TextInput(width=self.plot_settings.width_part(3, self.width))
        self.operation_input = RadioButtonGroup(labels=["<", "=", ">"], active=1,
                                                width=self.plot_settings.width_part(6, self.width))
        self.update_button = Button(label='Update',
                                    width=self.plot_settings.width_part(6, self.width))
        # todo: dynamically adjust width of buttons.
        #       Maybe create method to deal with this in general

        self.update_button.js_on_click(CustomJS(args=dict(source=source), code="source.change.emit()"))
        
        self.cds_view = CDSView(source=source, filters=[self.get_filter()])
        return

    @property
    def layout(self):
        return [self.column_input, self.operation_input, self.value_input, self.update_button]

    @property
    def source_n_view(self) -> dict:
        return {'source': self.cds_view.source, 'view': self.cds_view}

    def get_filter(self) -> CustomJSFilter:
        """ create a filter for actual data.

        Group Filters couldn't been updated nicely and wouldn't have </> capability.
        Boolean Filters would need access to source to update ... which is given ...

        todo: think about multiple filters
              could either be by extending java code or combine multiple
        """
        return CustomJSFilter(args=dict(column=self.column_input,
                                        entry=self.value_input,
                                        operator=self.operation_input), code="""
                    const indices = []
                    for (var i = 0; i < source.get_length(); i++) {
                        if ((column.value == "") || (entry.value == "")) {
                            indices.push(true)
                        } else if ((operator.active == 0) && (source.data[column.value][i] < entry.value)){
                            indices.push(true)
                        } else if ((operator.active == 1) && (source.data[column.value][i] == entry.value)){
                            indices.push(true)
                        } else if ((operator.active == 2) && (source.data[column.value][i] > entry.value)){
                            indices.push(true)
                        } else {
                            indices.push(false)
                        }
                    }
                    return indices
                """)
