import numpy as np

from typing import Callable

from bokeh.plotting import figure
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Select, Slider, Button

from bokeh_scripts.plot_settings import GeneralSettings, FigureAndControls
from bokeh_scripts.plot_save import SaveControls


class HistogramPlot (FigureAndControls):

    _source = ColumnDataSource({'value': [], 'left': [], 'right': [], 'center': []})

    # create figure
    fig = figure(title='Histogram', x_axis_label='Value', y_axis_label='Frequency',
                 tools="pan,box_zoom,wheel_zoom,save,reset,hover",
                 toolbar_location="right", active_scroll="wheel_zoom",
                 tooltips=[("Value", " @center"),
                           ("Frequency", " @value")])

    # add glyphs to show values
    fig.quad(top='value', bottom=0, left='left', right='right', source=_source)

    # add setting widgets
    select_content = Select(title="Histogram:")
    button_next = Button(label='Next')
    select_method = Select(title="Binning Method", value='auto', options=['fixed', 'auto', 'fd', 'doane', 'scott',
                                                                  'stone', 'rice', 'sturges', 'sqrt'])
    slider_bins = Slider(start=1, end=14, value=4.2, step=.1, title='Bins=2^', visible=False)

    save_controls = SaveControls(fig)

    def __init__(self, get_values_fun: Callable[[str], list], options: list, option_default: str = None,
                 width: int = None, width_controls: int = None, height: int = None,
                 plot_settings: GeneralSettings = GeneralSettings()):
        """ Create a Histogram with Select-Widget to choose from different options.

        "get_data_fun(selected_option)" will be called to collect the data.

        :param get_values_fun: function to handle dynamical data (will be called with label and returns list of data)
        :param options: possible label to select_content
        :param option_default: initial label
        """

        # deal with sizes (updates everything in second layout column
        #         #   last item is list and has to be skipped
        super().__init__(plot_main=self.fig, plot_controls=self.layout[1][:-1],
                         width=width, width_controls=width_controls, height=height, ratio_wh=2.5,
                         plot_settings=plot_settings)

        self.select_content.value = option_default
        self.select_content.options = options

        # update histogram
        def select_content_cb(_attr, _old, new):

            if self.select_method.value == 'fixed':
                bins = int(2 ** self.slider_bins.value)
            else:
                bins = self.select_method.value

            self._update_source(get_values_fun(new), bins=bins)

            self.fig.xaxis.axis_label = new
            self.fig.title.text = "Histogram of {name} ({count} entries in total)".format(
                name=new, count=sum(self._source.data['value']))
            self.save_controls.filename = "plotHist({name})".format(name=new)

        self.select_content.on_change('value', select_content_cb)

        # click trough content
        def next_cb(_event):
            idx = self.select_content.options.index(self.select_content.value) + 1
            if idx == len(self.select_content.options):
                idx = 0
            self.select_content.value = self.select_content.options[idx]

        self.button_next.on_click(next_cb)

        # handle method selection
        def select_method_cb(_attr, _old, new):
            if new == 'fixed':
                self.slider_bins.visible = True
            else:
                self.slider_bins.visible = False
            select_content_cb(None, None, self.select_content.value)

        self.select_method.on_change('value', select_method_cb)

        # handle bins selection
        def slider_bins_cb(_attr, _old, _new):
            select_content_cb(None, None, self.select_content.value)

        self.slider_bins.on_change('value', slider_bins_cb)

        # update trigger
        def update_plot():
            """Update Histogram manually (e.g when dynamic data has changed)"""
            select_content_cb(None, None, self.select_content.value)

        # make it a class function
        self.update = update_plot

        # trigger first update
        if option_default is not None:
            update_plot()

        return

    @property
    def layout(self):
        return [self.fig, [self.select_content, self.button_next, self.select_method, self.slider_bins,
                           self.save_controls.layout]]

    def _update_source(self, values: list, bins: (str, float) = 'auto') -> None:
        """
        Update the CDS with histogram data for given values
        :param values: values to show (only float or int entries are used, nan is ignored)
        :param bins: number or select_method (see link) to calculate bins
        <https://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram_bin_edges.html#numpy.histogram_bin_edges>
        """
        counts, edges = np.histogram([value for value in values if isinstance(value, (int, float)) and value == value],
                                     bins=bins)

        centers = [(left + right) / 2 for (left, right) in zip(edges[:-1], edges[1:])]

        self._source.data = {'value': counts, 'left': edges[:-1], 'right': edges[1:], 'center': centers}
        return
