"""
Specific Errors to track.
"""


class DataError(Exception):
    """Exception for missing data."""
    pass
