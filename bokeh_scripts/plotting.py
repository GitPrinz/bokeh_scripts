import colorsys

import numpy as np

# noinspection PyUnresolvedReferences
from bokeh.palettes import Category10, Category20, PRGn, RdBu, RdGy, RdYlBu, colorblind,\
    viridis, Turbo256, magma, inferno, gray, cividis
from bokeh.models import ColorMapper, CategoricalColorMapper, LinearColorMapper, LogColorMapper

"""
Some general scripts to support Plotting
"""


def get_values(list_input: list, return_nan: str = None) -> list:
    if return_nan is None:
        return [value for value in list_input if isinstance(value, (int, float)) and value == value]

    elif return_nan == 'str':
        return [value if isinstance(value, (int, float)) and value == value else 'NaN' for value in list_input]
    elif return_nan in ['np', 'NaN', 'nan']:
        return [value if isinstance(value, (int, float)) and value == value else np.NaN for value in list_input]


def rgb2hex(color_rgb: (tuple, list)) -> str:
    """ transform 3 floats 0 to 1 to hex string"""
    return '#%02x%02x%02x' % (int(color_rgb[0] * 255),
                              int(color_rgb[1] * 255),
                              int(color_rgb[2] * 255))


def hsv2hex(hsv_value: tuple):
    return rgb2hex(colorsys.hsv_to_rgb(hsv_value[0], hsv_value[1], hsv_value[2]))


categorical_maps = ['default', 'colorblind8', 'Category10', 'Category20', 'PRGn11', 'RdBu11', 'RdGy11', 'RdYlBu11',
                    'viridis256', 'Turbo256', 'magma256', 'inferno256', 'gray256', 'cividis256']
continuous_maps = ['default', 'viridis256', 'Turbo256', 'magma256', 'inferno256', 'gray256', 'cividis256']


def get_mapper(choice: str, values2map: list, color_palette: str = 'default') -> ColorMapper:
    """
    :return: appropriate color mapper (bokeh)
    :param choice: 'Categorical' for distinct values or 'Linear', 'Log' for continuous values.
    :param values2map: list with values or categories to colorize
    :param color_palette: special color pallet
        - viridis: blue, green, yellow
        - Turbo256: black, blue, green, yellow, red
        - inferno: black, violette, red, orange, white
        - magma: black, violette, red, white (more pastel than inferno)
        - cividis: black, blue, gray, yellow
        - gray: black and white
        Only for categorial:
        - Colorblind up to 8
        - Category10 up to 1o
        - PRGn, RdBu, RdGy, RdYlBu up to 11
        - Category20 up to 20

    """

    if choice == 'Categorical':
        # only unique categories (and filter NaN)
        unique_types = [str(value) for value in sorted(list(set(values2map))) if value == value]

        if color_palette == 'default':
            palette = None
        elif color_palette == 'Turbo256':
            palette = Turbo256[len(unique_types)]
        elif color_palette == 'viridis256':
            palette = viridis(len(unique_types))
        elif color_palette == 'cividis256':
            palette = cividis(len(unique_types))
        elif color_palette == 'magma256':
            palette = magma(len(unique_types))
        elif color_palette == 'inferno256':
            palette = inferno(len(unique_types))
        elif color_palette == 'gray256':
            palette = gray(len(unique_types))
        elif len(unique_types) <= 8 and color_palette == 'Colorblind8':
            palette = colorblind[len(unique_types)]
        elif len(unique_types) <= 10 and color_palette == 'Category10':
            palette = Category10[len(unique_types)]
        elif len(unique_types) <= 11 and color_palette in ['PRGn11', 'RdBu11', 'RdGy11', 'RdYlBu11']:
            if color_palette == 'PRGn11':
                palette = PRGn[len(unique_types)]
            elif color_palette == 'RdBu11':
                palette = RdBu[len(unique_types)]
            elif color_palette == 'RdGy11':
                palette = RdGy[len(unique_types)]
            elif color_palette == 'RdYlBu11':
                palette = RdYlBu[len(unique_types)]
            else:
                raise Exception('The Programmer made an mistake ...')
        elif len(unique_types) <= 20 and color_palette in 'Category10':
            palette = Category20
        else:
            palette = None
            print('"{pal}" is no available color palette for {num} categories plot.'.format(pal=color_palette,
                                                                                            num=len(unique_types)))

        if palette is not None:
            color_list = palette
        elif len(unique_types) <= 2:
            color_list = ['red', 'green'][:len(unique_types)]
        elif len(unique_types) <= 10:
            color_list = Category10[len(unique_types)]
        elif len(unique_types) <= 20:
            color_list = Category20[len(unique_types)]
        elif len(unique_types) <= 256:
            color_list = viridis(len(unique_types))
        else:
            print('Warning: {} different colors demanded. Using hue color wheel.'.format(len(unique_types)))
            color_list = [hsv2hex((hue, 0.5, 1)) for hue in
                          [240/len(unique_types)*value for value in range(len(unique_types))]]

        return CategoricalColorMapper(factors=unique_types, palette=color_list)

    else:
        # Linear, Log

        # consider only values
        values = [num for num in values2map if isinstance(num, (int, float))]

        if len(values) == 0:
            min_val = 0
            max_val = 1
            print('Warning[Color Mapper]: No values for continuous plot. Maybe it is a string column')
        else:
            min_val = np.nanmin(values)
            max_val = np.nanmax(values)

        if color_palette in ['default', 'Turbo256']:
            palette = Turbo256
        elif color_palette == 'viridis256':
            palette = viridis(256)
        elif color_palette == 'cividis256':
            palette = cividis(256)
        elif color_palette == 'magma256':
            palette = magma(256)
        elif color_palette == 'inferno256':
            palette = inferno(256)
        elif color_palette == 'gray256':
            palette = gray(256)
        else:
            palette = Turbo256
            print('"{}" is no available color palette for continuous plot.'.format(color_palette))

        if choice == 'Linear':
            return LinearColorMapper(palette=palette, nan_color='magenta', low=min_val, high=max_val)

        elif choice == 'Log':
            return LogColorMapper(palette=palette, nan_color='magenta', low=min_val, high=max_val)

        else:
            raise Exception('"{}" is an unknown choice.'.format(choice))
