from typing import Union


class GeneralSettings:
    """ Handle general settings for interfaces

    Stores "width", "height" and "widget_pixel_distance".

    Grants access to fixed widths:

    - width_half
    - width_third
    - width_quarter
    - width_part(n)

    Grants access to duo of dynamic width determined by "duo_ratio":

    - width_main
    - width_side
    
    todo: it is confusing, which property belongs to which class

    """

    widget_pixel_distance: int = 10  # at least for Chrome

    def __init__(self, width: int = None, height: int = None):
        """ Creates and object with multiple settings.

        All parameters are optional.

        :param width: Width of interface.
        :param height: Height of interface.
        """

        self.width: int = width
        self.height: int = height

        return

    # fixed width #
    ###############

    def width_part(self, part: int, width: int = None) -> Union[int, None]:
        """ Return width of a fractional element (consider space inbetween widgets)

        :param part: fraction of full space
        :param width: optional width parameter to prefer before included with """
        if width is None:
            if self.width is None:
                return None
            else:
                width = self.width
        return (width - (part-1) * self.widget_pixel_distance) // part

    @property
    def width_half(self) -> Union[int, None]:
        return self.width_part(2)

    @property
    def width_third(self) -> Union[int, None]:
        return self.width_part(3)

    @property
    def width_quarter(self) -> Union[int, None]:
        return self.width_part(4)

    # Methods #
    ###########

    def handle_width_height(self, width: Union[int, None], height: Union[int, None],
                            ratio_wh: float = None, plot_object=None) -> tuple:
        """ Constructs width from height and ratio in between them or the other way around.

        All Parameters are optional.
        Missing Parameters will be backed up from GeneralSettings object.

        :param width:
        :param height:
        :param ratio_wh: ratio width/height
        :param plot_object: to directly apply on object with width and height (probably a bit to much)
        :return: width, height
        """

        # set default width/height
        if width is None:
            width = self.width
        if height is None:
            height = self.height

        # try to construct missing info by ratio
        if ratio_wh is not None:
            if width is None and height is not None:
                width = ratio_wh * height
            elif height is None and width is not None:
                height = width / ratio_wh

        # set object with values if wished
        if plot_object is not None:
            if width is not None:
                plot_object.width = width
            if height is not None:
                plot_object.height = height

        return width, height

    def set_width(self, plot_objects: list, width: int = None):
        """ Sets .width for multiple objects. Either the given value, or the Value from GeneralSettings. """
        if width is not None:
            for obj in plot_objects:
                obj.width = width
        elif self.width is not None:
            for obj in plot_objects:
                obj.width = self.width


# More specific Settings #
##########################
# todo: would be nice if they are children of plot_settings, but this makes everything confusing ...
# todo: would be nice if they had width and height, but this makes everything confusing ...

class FullColumn:
    """ For Interfaces filling one column.

    Properties
    - width
    """

    width: int = None

    def __init__(self, width: int = None, plot_settings: GeneralSettings = GeneralSettings()):

        self.plot_settings = plot_settings
        self.width, _ = self.plot_settings.handle_width_height(width, None)


class HalfColumns:
    """ For Interfaces filling half the available space.

    Properties
    - width_half
    """

    width_half: int = None

    def __init__(self, width: int = None, plot_settings: GeneralSettings = GeneralSettings()):

        self.plot_settings = plot_settings
        width, _ = self.plot_settings.handle_width_height(width, None)

        if width is not None:
            self.width_half = (width - self.plot_settings.widget_pixel_distance) // 2


class FigureAndControls:
    """ For Interfaces with a main part and a side part.
    
    Adds Properties:
    
    - width_main
    - width_controls
    
    Adds Methods:
    
    - set_width_side

    # todo: This is really confusing. height is maybe not good here
    """

    width_main: int = None
    width_controls: int = None

    def __init__(self, plot_main=None, plot_controls: list = None,
                 width: int = None, width_controls: int = None, ratio_main2width: float = None,
                 height: int = None, ratio_wh: float = None,
                 plot_settings: GeneralSettings = GeneralSettings()):
        """ Creates and object with plot_settings.
        
        All parameter are optional.
        
        :param plot_main: Main object to change.
        :param plot_controls: List of control objects to change.
        :param width: Total width (will be caught from plot_settings).
        :param width_controls: Width of controls.
        :param ratio_main2width: Ratio of main object to toal width.
        :param height: Height of object (will be caught from plot_settings).
        :param ratio_wh: Ratio total width to height.
        :param plot_settings: GeneralSettings with width and height.
        """

        self.plot_settings = plot_settings
        width, height = self.plot_settings.handle_width_height(width, height)

        # value finding logic #
        #######################

        if width_controls is None:
            if width is None:
                width_main = None
            else:
                # width is not None
                if ratio_main2width is None:
                    width_controls = width // 4
                    width_main = width_controls * 3 - self.plot_settings.widget_pixel_distance
                else:
                    # ratio_main2width is not None
                    width_main = int(round(ratio_main2width * width))
                    width_controls = width - width_main - self.plot_settings.widget_pixel_distance

        elif width is None:
            # width_controls is not None
            width_main = width_controls * 3
        else:
            # width_controls and width is not None
            width_main = width - width_controls - self.plot_settings.widget_pixel_distance

        self.width_main = width_main
        self.width_controls = width_controls

        # [optional] change given objects #
        ###################################

        # change size of object
        if plot_main is not None:
            if width_main is not None:
                plot_main.width = width_main
            if height is not None:
                plot_main.height = height
            elif ratio_wh is not None and width_main is not None:
                plot_main.height = int(width_main / ratio_wh)

        # change width of controls
        if plot_controls is not None:
            self.set_width_controls(plot_controls)

    def set_width_controls(self, control_objects: list):

        if self.width_controls is not None:
            for obj in control_objects:
                obj.width = self.width_controls
