from typing import Callable

from bokeh.models import ColumnDataSource, Toggle, CDSView
from bokeh.models.widgets import DataTable, TableColumn, CellFormatter, NumberFormatter, StringFormatter
from bokeh.models.widgets.markups import Div

from bokeh_scripts.plot_settings import GeneralSettings, FullColumn


class TablePlot(FullColumn):
    """ Handle bokeh Data Table """

    text_align = 'right'
    presets = {'year': {'width': 35, 'title': 'Year',
                        'formatter': NumberFormatter(format='0', text_align=text_align)},
               'latitude_deg': {'width': 70, 'title': 'Latitude [°]',
                                'formatter': NumberFormatter(format='0.000', text_align=text_align)},
               'longitude_deg': {'width': 70, 'title': 'Longitude [°]',
                                 'formatter': NumberFormatter(format='0.000', text_align=text_align)},
               '0.00': {'width': 50, 'formatter': NumberFormatter(format='0.00', text_align=text_align)},
               'int': {'width': 50, 'formatter': NumberFormatter(format='0', text_align=text_align)}
               }
    # todo: create a better more general way to handle presets

    def __init__(self, source: ColumnDataSource, get_columns: Callable[[], list], additional_presets: dict = None,
                 view: CDSView = None,
                 width: int = None, height: int = None, title: str = 'Table', column_width: int = 70,
                 plot_settings: GeneralSettings = GeneralSettings()):
        """
        :param source: ColumnDataSource to display in table
        :param get_columns: Method to get list of column names (optional as tuples: with format - see get_column_preset)
        :param additional_presets:
        :param view: Column Data Source View by Bokeh to control the visible lines
        :param width:
        :param height:
        :param title: Header of table. Default will be "Table"
        :param column_width: default width for all columns
        :param plot_settings: with width and height of table
        """

        super().__init__(width=width, plot_settings=plot_settings)

        if additional_presets is not None:
            if isinstance(additional_presets, dict):
                self.presets.update(additional_presets)
            else:
                raise Exception('additional_presets have to be dict! Got {}'.format(type(additional_presets)))

        if view is None:
            view = CDSView(source=source)

        self.table = DataTable(source=source, view=view,
                               width=600 if self.width is None else self.width,
                               height=400 if height is None else height,
                               selectable='checkbox', fit_columns=False)
        self.get_columns = get_columns
        self._title_obj = Div(text='{}'.format(title))
        self.column_width = column_width

        self.update_table()

        return

    def update_table(self):
        self.table.columns = self.prepare_table_columns(self.get_columns())
        return

    def prepare_table_columns(self, columns: list) -> list:

        table_columns = []

        for column in columns:
            if isinstance(column, TableColumn):
                table_columns.append(column)
            elif isinstance(column, str):
                table_columns.append(TableColumn(field=column, title=column, width=self.column_width))
            elif isinstance(column, tuple):
                table_columns.append(self.get_column_preset(*column))
            else:
                raise Exception('Got unknown Type for Column Definition: "{}"'.format(type(column)))

        return table_columns

    def get_column_preset(self, column: str, preset: (str, dict)):
        """ Generates a column description for bokehs table.

        - str preset will be selected from presets.
        - dict preset can contain 'title' [str], 'width' [int] and 'formatter' (str, int or float) """

        if isinstance(preset, str):
            preset = self.presets[preset].copy()

        if 'title' not in preset:
            preset['title'] = column

        if 'width' not in preset:
            preset['width'] = self.column_width

        if 'formatter' not in preset:
            # noinspection PyTypeChecker
            preset['formatter'] = CellFormatter()
        elif isinstance(preset['formatter'], type):
            preset['formatter'] = self.get_formatter(preset['formatter'])

        return TableColumn(field=column, title=preset['title'], width=preset['width'], formatter=preset['formatter'])

    def get_formatter(self, column_type):
        if column_type == str:
            return StringFormatter(text_align=self.text_align)
        elif column_type == int:
            return NumberFormatter(text_align=self.text_align, format='0')
        elif column_type == float:
            return NumberFormatter(text_align=self.text_align, format='0.000')
        else:
            return CellFormatter()

    @property
    def layout(self):
        return [[self._title_obj, self.table]]

    @property
    def title(self):
        return self._title_obj.text

    @title.setter
    def title(self, new_title: str):
        assert isinstance(new_title, str)
        self._title_obj.text = new_title


class TablePlotSwitchable(TablePlot):
    """ The Same as the TablePlot, but switchable/expandable by a button with the title. """

    def __init__(self, source: ColumnDataSource, get_columns: Callable[[], list], additional_presets: dict = None,
                 view: CDSView = None,
                 width: int = None, height: int = None, title: str = 'Table', column_width: int = 70,
                 visible: bool = True, plot_settings: GeneralSettings = GeneralSettings()):

        super().__init__(source=source, get_columns=get_columns, additional_presets=additional_presets,
                         view=view,
                         width=width, height=height, title=title, column_width=column_width,
                         plot_settings=plot_settings)

        self._title_obj = Toggle(label=title, active=visible, width=self.table.width)
        self.table.visible = visible

        def visible_switch_cb(_attr, _old, new):
            self.table.visible = new
        self._title_obj.on_change('active', visible_switch_cb)

    @property
    def title(self):
        return self._title_obj.label

    @title.setter
    def title(self, new_title: str):
        assert isinstance(new_title, str)
        self._title_obj.label = new_title
