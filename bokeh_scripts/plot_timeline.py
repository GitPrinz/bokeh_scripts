""" Create a timeline of some source """

from typing import Union

import numpy as np
import pandas as pd

from bokeh_scripts.plot_settings import GeneralSettings, FullColumn

from bokeh.plotting import figure, ColumnDataSource
from bokeh.models import LabelSet, HoverTool


class TimelinePlot(FullColumn):

    _source: ColumnDataSource = ColumnDataSource({'group': [],  # group of entries
                                                  'label': [],  # name of entry
                                                  'top': [],  # auto generated
                                                  'bottom': [],  # auto generated
                                                  'start': [],  # beginning of entry
                                                  'stop': []})  # end of entry
    fig: figure
    line_height: float = 0.9
    group_names: list = None

    def __init__(self,
                 time_data: Union[ColumnDataSource, pd.DataFrame, None],
                 group_names: list = None,
                 label_orientation: float = 0,
                 add_today: bool = True,
                 title_str: str = 'Timeline',
                 width: int = None, plot_settings: GeneralSettings = GeneralSettings()):
        """ Create a timeline plot from "group", "label",  and "start", "stop" data.

        start, stop is expected to be in timedate format.

        :param time_data: Bokeh ColumnDataSource or Pandas DataFrame
            Expected columns: "group", "label",  and "start", "stop"
            Optional columns: "fill_color", "line_color", "fill_alpha", 'line_alpha
        :param group_names: Which groups should be shown.
        :param label_orientation: Orientation of entry labels in degree.
        :param add_today: Show Line for Today
        :param title_str: Title of Plot
        :param width:
        :param plot_settings:
        """

        super().__init__(width=width, plot_settings=plot_settings)

        # Initializing Source
        # todo: put this in some method
        if isinstance(time_data, ColumnDataSource):
            self._source = time_data
        elif isinstance(time_data, pd.DataFrame):
            self._source = ColumnDataSource(time_data)
        elif time_data is not None:
            raise Exception('Unknown input type: {}'.format(type(time_data)))
        self.verify_main_columns()
        self.ensure_colors()

        if group_names is None:
            # get keywords for grouping
            self.group_names = sorted(list(set(self._source.data['group'])))
        else:
            self.group_names = list(reversed(group_names))
        self.first_date = min(self._source.data['start'])
        self.last_date = max(self._source.data['stop'])

        self.update_source()

        # Initializing figure
        self.fig = figure(title=title_str, width=self.width,
                          x_axis_type='datetime')
        self.fig.add_tools(HoverTool(
            tooltips=[("Group", " @group"),
                      ("Label", " @label"),
                      ("From", " @start{%F}"),
                      ("To", " @stop{%F}")],
            formatters={"@start": "datetime",
                        "@stop": "datetime"}
        ))

        # add tickmarks
        self.fig.yaxis.ticker = list(range(len(self.group_names)))
        self.fig.yaxis.major_label_overrides = {num: nam for num, nam in enumerate(self.group_names)}
        self.fig.y_range.start = -1

        # Add Range Glyphs
        self.fig.quad(top='top', bottom='bottom', left='start', right='stop', source=self._source,
                      fill_color='fill_color', fill_alpha='fill_alpha',
                      line_color='line_color', line_alpha='line_alpha')
        self.labels = LabelSet(x='start', y='bottom', text='label', text_color='line_color', source=self._source,
                               x_offset=8, angle=label_orientation/360*np.pi, text_baseline='middle',
                               y_offset=8, render_mode='canvas')  # label offset in text coordinates [pt]...
        self.fig.add_layout(self.labels)
        # Add today line
        if add_today:
            # noinspection PyArgumentList
            self.fig.line(x=[pd.Timestamp.today()] * 2,
                          y=[min(self._source.data['bottom']), max(self._source.data['top'])])

        return

    @property
    def layout(self):
        return self.fig

    def get_active_labels(self, time_stamp, group_str: str = None):
        """ Return the active labels at the current time stamp

        :param time_stamp: specific timestamp
        :param group_str: just check for specific group (optional)

        #1 Count if no group is given, or group is matching
        #2 Count if start is not given (active since ever) or start is before or equal to time_stamp
        #3 Count if stop is not given (active forever) or time_stamp is before or equal to stop - 1s
        #   -1 Second will not count events, which stop at the specific time.
        #   todo: does this work? it is kind of strange

        """
        return [lab for lab, group, start, stop in zip(self._source.data['label'],
                                                       self._source.data['group'],
                                                       self._source.data['start'],
                                                       self._source.data['stop'])
                if (True if group is None else group == group_str) and  # 1
                ((start != start or start <= time_stamp) and  # 2
                 (stop != stop or time_stamp <= stop - np.timedelta64(1, 's')))  # 3
                ]

    def verify_main_columns(self) -> None:
        """ Short check if every necessary column is there """
        if "group" not in self._source.data:
            raise Exception('"group" column is missing')
        if "label" not in self._source.data:
            raise Exception('"label" column is missing')
        if "start" not in self._source.data:
            raise Exception('"start" column is missing')
        if "stop" not in self._source.data:
            raise Exception('"stop" column is missing')
        return

    def ensure_colors(self) -> None:
        """ Verify color columns exist """
        if "fill_color" not in self._source.data:
            self._source.data['fill_color'] = ['lightgray'] * len(self._source.data['label'])
        if "line_color" not in self._source.data:
            self._source.data['line_color'] = ['black'] * len(self._source.data['label'])
            self.color_timeless_events()
        if "fill_alpha" not in self._source.data:
            self._source.data['fill_alpha'] = [0.5] * len(self._source.data['label'])
        if "line_alpha" not in self._source.data:
            self._source.data['line_alpha'] = [1] * len(self._source.data['label'])

    def color_timeless_events(self, line_color: str = 'red'):
        """ change line color of instant events """

        new_line_colors = []

        for start, stop, old_color in zip(
                self._source.data['start'], self._source.data['stop'], self._source.data['line_color']):
            if start == stop:
                new_line_colors.append(line_color)
            else:
                new_line_colors.append(old_color)

        self._source.data['line_color'] = new_line_colors

    def update_source(self):
        """ Update top and bottom entry in source. """

        # get data in ordered way
        starts, stops, groups, order = zip(*[(sta, sto, gro, idx) for sta, sto, gro, idx in sorted(zip(
            self._source.data['start'], self._source.data['stop'], self._source.data['group'],
            list(range(len(self._source.data['start'])))
        ))])

        top = []
        bottom = []

        active_labels = {group: [] for group in self.group_names}
        for start, stop, group in zip(starts, stops, groups):

            if group not in self.group_names:  # bad hack
                top.append(-10)
                bottom.append(-10)
                continue

            if start == stop:
                # moment in time
                top.append(self.group_names.index(group) + self.line_height / 2)
                bottom.append(top[-1] - self.line_height)

            else:
                # time range

                # store future (or never) ending blocks
                active_labels[group] = [lab for lab in active_labels[group] if lab >= start or lab != lab]

                num_labels = max([len(self.get_active_labels(start, group)),
                                  len(self.get_active_labels(stop, group)), 1])
                height = self.line_height/num_labels
                top.append(len(active_labels[group])*height + self.group_names.index(group) + height/2)
                bottom.append(top[-1] - height)

                active_labels[group].append(stop)

        top, bottom = zip(*[(to, bo) for _, to, bo in sorted(zip(order, top, bottom))])

        # noinspection PyArgumentList
        self._source.data.update({'start': [sta if sta == sta else self.first_date
                                            for sta in self._source.data['start']],
                                  'stop': [sto if sto == sto else pd.Timestamp.today()
                                           for sto in self._source.data['stop']],
                                  'top': top,
                                  'bottom': bottom})

        return








