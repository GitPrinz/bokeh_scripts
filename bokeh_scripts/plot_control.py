import re

from bokeh_scripts.plot_settings import GeneralSettings, FullColumn

from bokeh.models import TextInput, Button
from bokeh.models import Slider
from bokeh.models import Toggle
from bokeh.plotting import curdoc

small_button_width = 65


class PlotControl(FullColumn):
    """ Induce code calls in your Interface.

    Warning: This is a weak point and should only used by trusted clients
    """

    code_input = TextInput(title='Code Input (Warning: This can crash the interface)')
    run_button = Button(label='Execute')

    def __init__(self, eval_function: callable,
                 width: int = None, plot_settings: GeneralSettings = GeneralSettings()):
        """ Create PlotControl Widget

        :param eval_function: expects python lines to execute in the main file
        :param width: width of widget
        :param plot_settings: general plot settings
        """

        super().__init__(width=width, plot_settings=plot_settings)

        self.plot_settings = plot_settings
        self.update_width(width)

        self.eval_function = eval_function

        def run_button_cb(_event):
            self.execute()

        self.run_button.on_click(run_button_cb)

        return

    @property
    def layout(self):
        return [self.code_input, self.run_button]

    def update_width(self, new_width: int = None):
        width, _ = self.plot_settings.handle_width_height(new_width, None)
        if width is not None:
            # | run_button | code_input (sbw > 1/3)|
            self.run_button.width = min([small_button_width,
                                         (width - self.plot_settings.widget_pixel_distance) / 3])
            self.code_input.width = width - self.plot_settings.widget_pixel_distance - self.run_button.width

    def execute(self):
        self.eval_function(self.code_input.value)


class ScriptControl(PlotControl):

    #: list of lines
    script_list: list = None

    # file loading
    file_select = TextInput(title='Select script to load')
    load_file_button = Button(label='Load')

    # file execution
    skip_button = Button(label='Next')
    run_skip_button = Button(label='Execute and Next')
    line_slider = Slider(title='Line', start=0, end=1, value=1, step=1)

    def __init__(self, eval_function: callable,
                 width: int = None, plot_settings: GeneralSettings = GeneralSettings()):
        super().__init__(eval_function=eval_function, width=width, plot_settings=plot_settings)
        self.update_width(width)  # in init the super routine of update_width is called!?

        def load_file_button_cb(_event):
            with open(self.file_select.value) as f:
                self.script_list = f.readlines()
            self.line_slider.end = len(self.script_list)
            self.line_slider.value = 0

        self.load_file_button.on_click(load_file_button_cb)

        def line_slider_cb(_attr, _old, new):
            if new == 0:
                new = 1
            self.code_input.value = self.script_list[new-1]

        self.line_slider.on_change('value', line_slider_cb)

        def skip_button_cb(_event):
            self.go_next()

        self.skip_button.on_click(skip_button_cb)

        def run_skip_button_cb(_event):
            self.execute_and_next()

        self.run_skip_button.on_click(run_skip_button_cb)

        return

    @property
    def layout(self):
        return [[[self.code_input, self.run_button],
                 [self.line_slider, self.skip_button, self.run_skip_button],
                 [self.file_select, self.load_file_button, ]
                 ]]

    def update_width(self, new_width: int = None):
        width, _ = self.plot_settings.handle_width_height(new_width, None)
        super().update_width(width)
        if width is not None:
            # | line_slider | skip_button (120 > 1/6) | run_skip_button (sbw > 1/6) |
            self.skip_button.width = min([small_button_width,
                                          (width - self.plot_settings.widget_pixel_distance) / 6])
            self.run_skip_button.width = min([120,
                                              (width - self.plot_settings.widget_pixel_distance) / 6])
            self.line_slider.width = \
                width - self.plot_settings.widget_pixel_distance * 2 \
                - self.skip_button.width - self.run_skip_button.width
            # | file_select | load_file_button (swbw > 1/6) |
            self.load_file_button.width = min([small_button_width,
                                               (width - self.plot_settings.widget_pixel_distance) / 6])
            self.file_select.width = width - self.plot_settings.widget_pixel_distance - self.load_file_button.width

    def go_next(self):
        if self.line_slider.value < self.line_slider.end:
            self.line_slider.value += 1
        else:
            self.line_slider.value = 0

    def execute_and_next(self):
        self.eval_function(self.code_input.value)
        self.go_next()


class MetroControl(ScriptControl):

    waiting_slider = Slider(title='Waiting Time', start=0.1, end=10, value=2, step=0.1)
    auto_toggle = Toggle(label='Automatic execution', active=False)
    _current_callback = None

    def __init__(self, eval_function: callable,
                 width: int = None, plot_settings: GeneralSettings = GeneralSettings()):
        super().__init__(eval_function=eval_function, width=width, plot_settings=plot_settings)
        self.update_width(width)  # in init the super routine of update_width is called!?
        # todo: the above is stupid

        def auto_toggle_cb(_attr, _old, new):
            self.switch_callback(new)

        self.auto_toggle.on_change('active', auto_toggle_cb)

        return

    @property
    def layout(self):
        super_layout = super().layout
        return [[[super_layout[0][:2]],
                 [self.waiting_slider, self.auto_toggle],
                 [super_layout[0][2:]]
                 ]]

    def update_width(self, new_width: int = None):
        width, _ = self.plot_settings.handle_width_height(new_width, None)
        super().update_width(width)
        if width is not None:
            # | waiting_slider | auto_toggle (120 > 1/6) |
            self.auto_toggle.width = min([120,
                                          (width - self.plot_settings.widget_pixel_distance) / 6])
            self.waiting_slider.width = width - self.plot_settings.widget_pixel_distance - self.auto_toggle.width

    def switch_callback(self, state_bool: bool):
        if state_bool:
            self.execute_and_next()
            self.add_delayed_callback(self.execute_and_next, self.waiting_slider.value * 1000)
        else:
            self.remove_callback()

    def add_delayed_callback(self, fun: callable, delay_ms: int):
        self._current_callback = curdoc().add_periodic_callback(fun, delay_ms)

    def remove_callback(self):
        if self._current_callback is not None:
            curdoc().remove_periodic_callback(self._current_callback)
            self._current_callback = None


class DynamicControl(MetroControl):
    """ Will execute every line directly and look for WAIT"""

    execute_till_next_button = Button(label='Execute till next')

    def __init__(self, eval_function: callable,
                 width: int = None, plot_settings: GeneralSettings = GeneralSettings()):
        super().__init__(eval_function=eval_function, width=width, plot_settings=plot_settings)
        self.update_width(width)  # in init the super routine of update_width is called!?
        # todo: the above is stupid

        self.waiting_slider.title = 'Additional waiting time (for slow machines)'
        self.waiting_slider.start = 0
        self.waiting_slider.value = 0

        def execute_and_next_cb(_event):
            self.execute_till_next()

        self.execute_till_next_button.on_click(execute_and_next_cb)

        return

    @property
    def layout(self):
        super_layout = super().layout
        # noinspection PyTypeChecker
        super_layout[0][1].append(self.execute_till_next_button)
        return super_layout

    def update_width(self, new_width: int = None):
        width, _ = self.plot_settings.handle_width_height(new_width, None)
        super().update_width(width)
        if width is not None:
            # | waiting_slider | auto_toggle (120 > 1/6) |
            self.execute_till_next_button.width = min([120,
                                                       (width - self.plot_settings.widget_pixel_distance * 2) / 6])
            self.waiting_slider.width = \
                width - self.plot_settings.widget_pixel_distance * 2 \
                - self.auto_toggle.width - self.execute_till_next_button.width

    def switch_callback(self, state_bool: bool):
        if state_bool:
            self.execute_and_wait()
        else:
            self.remove_callback()

    def execute_till_next(self):
        self.remove_callback()
        self.execute_and_next()
        if '#WAIT' not in self.code_input.value and '#EOF' not in self.code_input.value:
            self.execute_till_next()

    def execute_and_wait(self):
        self.remove_callback()
        self.execute_till_next()
        if '#WAIT' in self.code_input.value:
            self.add_delayed_callback(self.execute_and_wait,
                                      self.waiting_slider.value * 1000
                                      + int(re.search('#WAIT(\d+)', self.code_input.value)[1]))
        elif '#EOF' in self.code_input.value:
            self.auto_toggle.active = False
        else:
            raise Exception('This line should not be reachable')
