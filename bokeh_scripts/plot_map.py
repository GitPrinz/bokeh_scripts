import os

import pandas as pd
import numpy as np

from bokeh.models import GeoJSONDataSource, ColumnDataSource
from bokeh.models import LinearColorMapper, ColorBar
from bokeh.models.tools import LassoSelectTool
from bokeh.plotting import figure

from bokeh_scripts.plot_settings import GeneralSettings


def prepare_countries() -> GeoJSONDataSource:
    """ get an earth map as GeoJSONDataSource

    Data is stored in the package and was provided by <https://geojson-maps.ash.ms/>
    """
    with open(os.path.join(os.path.dirname(__file__), "data/worldmap.geo.json"), "r") as fid:
        json_data = fid.read()
    return GeoJSONDataSource(geojson=json_data)


def prepare_some_cities() -> ColumnDataSource:
    return ColumnDataSource({'longitude_deg': [8.806422],
                             'latitude_deg': [53.073635],
                             'type': ['city'],
                             'id': ['Bremen']})


class MapPlot:

    source_countries = prepare_countries()
    source_places = prepare_some_cities()
    source_stations = ColumnDataSource()
    glyph_stations = None
    source_lines = None
    glyph_lines = None
    color_mapper = None

    default_sample_size = 6
    default_station_size = 10
    default_place_size = 10

    map = figure(title='Worldmap (Scale and Selectable)',
                 x_axis_label='Longitude [deg]', y_axis_label='Latitude [deg]',
                 background_fill_color='lightblue',
                 tools="pan,box_zoom,wheel_zoom,save,reset",
                 toolbar_location="above", active_scroll="wheel_zoom",
                 tooltips=[("Type", " @type"),
                           ("Id", " @id")])
    map.xgrid.grid_line_color = None
    map.ygrid.grid_line_color = None

    def __init__(self,
                 source_samples: ColumnDataSource,
                 source_stations: ColumnDataSource = None,
                 width: int = None, height: int = None,
                 show_places: bool = True,
                 scale_samples: bool = False,
                 plot_settings: GeneralSettings = GeneralSettings()):
        """

        :param source_samples: Bokeh's ColumnDataSource with at least 'latitude_deg' and 'longitude_deg'
        :param source_stations: Optional CDS with Station positions
        :param width:
        :param height:
        :param show_places: Show some default places as red dots (collected by prepare_some_cities())
        :param scale_samples: Scale samples according to the frequency of their position (among all samples).
        :param plot_settings:
        """

        self.plot_settings = plot_settings
        width, height = self.plot_settings.handle_width_height(width, height, ratio_wh=4/3)

        if width is not None:
            self.map.plot_width = width
        if height is None:
            self.map.plot_height = height

        # Add patch renderer to figure to show map.
        self.map.patches('xs', 'ys', source=self.source_countries,
                         line_color='gray', line_width=0.25, fill_alpha=1, fill_color='lightgray')

        # Add Samples
        self.source_samples = source_samples
        self.glyph_samples = self.add_samples()

        # Add Stations

        if source_stations is not None:
            self.source_stations = source_stations
            self.glyph_stations = self.add_stations()

        # Styling

        self.style_map()

        if show_places:
            self.glyph_places = self.add_places()
        else:
            self.glyph_places = None

        if scale_samples:
            self.scale_by_occurence()

        return

    def add_samples(self):
        return self.map.circle(x='longitude_deg', y='latitude_deg', source=self.source_samples,
                               legend_label='Samples', line_color='orange',
                               size=self.default_sample_size)

    def add_places(self):
        return self.map.circle(x='longitude_deg', y='latitude_deg', source=self.source_places,
                               fill_color='red', legend_label='Cities',
                               size=self.default_place_size)

    def add_stations(self):
        return self.map.square(x='longitude_deg', y='latitude_deg', source=self.source_stations,
                               legend_label='Stations', line_color='orange',
                               size=self.default_station_size)

    def add_lines(self, sample_column: str, station_column: str):
        self.source_lines = ColumnDataSource(get_station_sample_lines_source(self.source_samples, sample_column,
                                                                             self.source_stations, station_column))
        self.glyph_lines = self.map.multi_line('xs', 'ys', source=self.source_lines,
                                               line_color='black', legend_label='Station to Sample',
                                               line_dash='dotted')
        return

    # Additional Settings #
    #######################

    def style_map(self):
        self.map.legend.location = "top_left"
        self.map.legend.click_policy = "hide"

        self.map.hover.tooltips.extend([("Latitude [°]:", " @latitude_deg"),
                                        ("Longitude [°]:", " @longitude_deg")])
        return

    def add_colorbar(self, template: str, field_samples: str = None, field_stations: (str, bool) = None,
                     title: str = None):

        if template == 'temperature_degC':
            palette = ['#084594', '#2171b5', '#4292c6', '#6baed6',
                       '#9ecae1', '#c6dbef', '#deebf7', '#f7fbff']
        else:
            raise Exception('Type "{}" is not yet defined.'.format(template))

        if field_samples is None:
            field_samples = template

        if field_stations is None or (isinstance(field_stations, bool) and field_stations):
            field_stations = field_samples

        if title is None:
            title = field_samples

        # add colormap
        self.color_mapper = LinearColorMapper(palette=palette, nan_color='magenta',
                                              low=np.nanmin([num for num in self.source_samples.data[field_samples]
                                                             if isinstance(num, (int, float))]),
                                              high=np.nanmax([num for num in self.source_samples.data[field_samples]
                                                              if isinstance(num, (int, float))]))
        self.map.add_layout(ColorBar(color_mapper=self.color_mapper, location=(0, 0),
                                     title=title, title_standoff=15), "right")

        self.glyph_samples.glyph.fill_color = {"field": field_samples, "transform": self.color_mapper}
        if not isinstance(field_stations, bool) and self.glyph_stations is not None:
            self.glyph_stations.glyph.fill_color = {"field": field_stations, "transform": self.color_mapper}

        return

    def scale_by_occurence(self):
        add_position_occurrence(self.source_samples)
        self.source_samples.add(self.source_samples.data['position_occurrence'] + self.default_sample_size - 1,
                                'symbol_size')
        self.glyph_samples.glyph.size = 'symbol_size'

        if self.glyph_stations is not None:
            add_position_occurrence(self.source_stations)
            self.source_stations.add(self.source_stations.data['position_occurrence'] + self.default_station_size - 1,
                                     'symbol_size')
            self.glyph_stations.glyph.size = 'symbol_size'
        return

    def add_lasso(self, active: bool = True):
        lasso_select = LassoSelectTool()
        self.map.add_tools(lasso_select)
        if active:
            self.map.toolbar.active_drag = lasso_select
            return


def add_position_occurrence(source_data: ColumnDataSource):
    """
    Scan for identical longitude_deg and latitude_deg in source_data
    and add column "position_occurrence" with number of identical positions.

    This is a rather slow function.

    :param source_data: ColumnDataSource
    :return:
    """

    source_frame = source_data.to_df()
    occurrence = source_frame['longitude_deg'] * 0

    for long in list(set(source_frame['longitude_deg'])):
        for lat in list(set(source_frame.loc[source_frame['longitude_deg'] == long]['latitude_deg'])):
            occurrence[source_frame.index[(source_frame['longitude_deg'] == long)
                                          & (source_frame['latitude_deg'] == lat)]] = \
                len(source_frame.loc[(source_frame['longitude_deg'] == long) & (source_frame['latitude_deg'] == lat)])

    source_data.add(occurrence, 'position_occurrence')
    return


def get_station_sample_lines_source(sample_source: ColumnDataSource, sample_column: str,
                                    station_source: ColumnDataSource, station_column: str):

    return get_station_sample_lines(sample_source.to_df().set_index(sample_column),
                                    station_source.to_df().set_index(station_column))


def get_station_sample_lines(sample_frame: pd.DataFrame, station_frame: pd.DataFrame) -> dict:
    """
    Create table with lines from stations to samples
    WARNING: Input will be manipulated

    :param sample_frame: with index as sample_id and columns 'id', 'latitude_deg', 'longitude_deg'
    :param station_frame: with index as sample_id and columns 'id', 'latitude_deg', 'longitude_deg'
    :return: dict with xs, yx, type, id({id_station}_{id_sample})
    """
    sample_frame.drop(columns=list(set(sample_frame.columns)-{'id', 'latitude_deg', 'longitude_deg'}), inplace=True)
    sample_frame.replace("", pd.NA, inplace=True)
    sample_frame.dropna(axis=0, how='any', inplace=True)
    station_frame.drop(columns=list(set(station_frame.columns) - {'id', 'latitude_deg', 'longitude_deg'}), inplace=True)
    station_frame.replace("", pd.NA, inplace=True)
    station_frame.dropna(axis=0, how='any', inplace=True)

    if 'id' not in station_frame:
        station_frame['id'] = station_frame.index

    line_frame = sample_frame.join(station_frame, how='inner', lsuffix='_sample', rsuffix='_station')

    line_frame['type'] = 'Sample line'

    return {'xs': [[station, sample] for (station, sample)
                   in zip(line_frame['longitude_deg_station'], line_frame['longitude_deg_sample'])],
            'ys': [[station, sample] for (station, sample)
                   in zip(line_frame['latitude_deg_station'], line_frame['latitude_deg_sample'])],
            'type': line_frame['type'].tolist(),
            'id': ["{id_station}_{id_sample}".format(id_station=station, id_sample=sample) for (station, sample)
                   in zip(line_frame['id_station'], line_frame['id_sample'])]}
