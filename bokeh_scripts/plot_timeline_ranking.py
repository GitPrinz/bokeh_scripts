""" Create a timeline of some source """

from typing import Union

import numpy as np
import pandas as pd

from bokeh_scripts.plot_settings import GeneralSettings, FullColumn

from bokeh.plotting import ColumnDataSource
from bokeh.palettes import Set1

from bokeh_scripts.plot_timeline import TimelinePlot


def scale_values(value_list: list, new_min: float = 0, new_max: float = 1) -> list:
    """  scale values for a specific range

    :param value_list: values
    :param new_min: new minimum (default 0)
    :param new_max: new maximum (default 1)

    todo: not yet able to deal with nans
        probably this mehtod exists and could be replaced
    """

    min_value = min(value_list)
    max_value = max(value_list)

    new_list = [(value - min_value) / (max_value - min_value) * (new_max - new_min) + new_min
                for value in value_list]

    return new_list


class TimelineRankingPlot(FullColumn):

    _source_ranking: ColumnDataSource = ColumnDataSource({'label': [],  # labels of entries
                                                          'description': [],  # description of entries
                                                          'intensity': [],  # intensity of entry
                                                          'date': []})  # date of entry

    _source_timeline: ColumnDataSource = ColumnDataSource({'group': [],  # group of entries
                                                           'label': [],  # name of entry
                                                           'start': [],  # beginning of entry
                                                           'stop': [],  # end of entry
                                                           'fill_color': [],  # color of box
                                                           'fill_alpha': [],  # opacity of box
                                                           'line_alpha': [],  # opacity of lines
                                                           })

    def __init__(self,
                 event_data: Union[ColumnDataSource, pd.DataFrame, None],
                 num_days: int = 7,
                 num_ranks: int = 5,
                 label_keys: list = None,
                 label_orientation: float = np.pi/2 * 0.9,
                 title_str: str = None,
                 add_today: bool = True,
                 width: int = None, plot_settings: GeneralSettings = GeneralSettings()):
        """ Create a timeline plot from "labeling", "description",  and "date" data.

        :param event_data: Bokeh ColumnDataSource or Pandas DataFrame
        :param num_days: Number of days to rank
        :param label_keys: Which keys should be searched and shown.
        :param label_orientation: Orientation of entry labels.
        :param add_today: Show Line for Today
        :param title_str: Title of Plot
        :param width:
        :param plot_settings:
        """

        super().__init__(width=width, plot_settings=plot_settings)

        self.num_days = num_days

        if title_str is None:
            title_str = 'Timeline: Ranking in {num} day blocks (opacity by intensity, line by frequency)'.format(
                num=self.num_days)

        # Initializing Source
        if isinstance(event_data, ColumnDataSource):
            self._source_ranking = event_data
        elif isinstance(event_data, pd.DataFrame):
            self._source_ranking = ColumnDataSource(event_data)
        elif event_data is not None:
            raise Exception('Unknown input type: {}'.format(type(event_data)))

        if label_keys is None:
            # get keywords for grouping
            self.label_keys = sorted(list(set(self._source_ranking.data['labeling'])))
        else:
            self.label_keys = label_keys

        self.rank_keys = ['Rank {}'.format(num+1) for num in range(num_ranks)]

        self.update_source()

        self.timeline = TimelinePlot(time_data=self._source_timeline,
                                     group_names=self.rank_keys,
                                     label_orientation=label_orientation,
                                     add_today=add_today,
                                     title_str=title_str,
                                     width=width,
                                     plot_settings=plot_settings)
        return

    @property
    def fig(self):
        return self.timeline.fig

    @property
    def layout(self):
        return self.fig

    def update_source(self):
        """ Update top and bottom entry in source. """

        print('Doing inefficient scan. This may take some time.')

        date_start = min(self._source_ranking.data['date'])
        date_end = max(self._source_ranking.data['date'])

        key_dict = {key: 0 for key in self.label_keys}

        date_active = date_start
        while date_active < date_end:

            date_inactive = date_active + np.timedelta64(self.num_days, 'D')

            # Brute Force - fckng inefficient
            active_entries = [
                (label, description, intensity) for label, description, intensity, date
                in zip(self._source_ranking.data['label'], self._source_ranking.data['description'],
                       self._source_ranking.data['intensity'], self._source_ranking.data['date'])
                if date_active <= date < date_inactive
            ]

            if len(active_entries) > 0:
                active_labels, active_descriptions, active_intensities = zip(*active_entries)

                # check all keys and collect intensities
                current_intensities = dict()
                for key in self.label_keys:

                    new_intensities = []
                    for label, intensity in zip(active_labels, active_intensities):
                        if key in label:
                            new_intensities.append(intensity)

                    if len(new_intensities) > 0:
                        current_intensities[key] = new_intensities

                if len(current_intensities) > 0:

                    # rank intensities
                    name_list = []
                    intensity_list = []
                    rank_list = []
                    for key, intensities in current_intensities.items():

                        key_dict[key] += len(intensities)
                        rank_list.append(len(intensities))
                        intensity_list.append(sum(intensities)/len(intensities))

                        name_list.append('{num} {key} [{min:.0f}, {mean:.1f}, {max:.0f}]'.format(
                            num=len(intensities), key=key,
                            min=min(intensities), mean=intensity_list[-1], max=max(intensities)
                        ))

                    ranked_names, ranked_intensities, ranked_numbers = zip(*[
                        (nam, alp, num) for num, alp, nam in sorted(zip(
                            rank_list, intensity_list, name_list), reverse=True)])

                    groups = self.rank_keys[:len(ranked_names)]
                    labels = ranked_names[:len(self.rank_keys)]
                    ints = ranked_intensities[:len(self.rank_keys)]
                    nums = ranked_numbers[:len(self.rank_keys)]
                    # add to timeline_source
                    self._source_timeline.stream({'group': groups,  # rank_title
                                                  'label': labels,  # name of entry
                                                  'start': [date_active] * len(groups),  # beginning of entry
                                                  'stop': [date_inactive - np.timedelta64(1, 's')] * len(groups),
                                                  'fill_color': ['lightgray'] * len(groups),
                                                  'fill_alpha': ints,
                                                  'line_alpha': nums})

            date_active = date_inactive

        # this is probably not a good way: filling a source and rewriting later
        # noinspection PyTypeChecker
        self._source_timeline.data['fill_alpha'] = scale_values(self._source_timeline.data['fill_alpha'],
                                                                new_min=0, new_max=1)
        # noinspection PyTypeChecker
        self._source_timeline.data['line_alpha'] = scale_values(self._source_timeline.data['line_alpha'],
                                                                new_min=0, new_max=1)

        self.update_colors(key_dict)

        return

    def update_colors(self, key_dict: dict, adapt_labels: bool = False) -> None:
        """ Change color according to entries """

        color_pallet = Set1[8]

        ranked_frequency, ranked_key = zip(*sorted(zip(key_dict.values(), key_dict.keys()), reverse=True))

        color_dict = {key: color for key, color in zip(ranked_key[:len(color_pallet)], color_pallet)}

        tl_labels = self._source_timeline.data['label']
        tl_colors = self._source_timeline.data['fill_color']

        for major_key in color_dict:

            new_labels = []
            new_colors = []

            for tl_label, tl_color in zip(tl_labels, tl_colors):

                if major_key in tl_label:
                    if adapt_labels:
                        new_labels.append(tl_label.replace(' {}'.format(major_key), ''))
                    else:
                        new_labels.append(tl_label)
                    new_colors.append(color_dict[major_key])
                else:
                    new_labels.append(tl_label)
                    new_colors.append(tl_color)

            tl_labels = new_labels
            tl_colors = new_colors

        self._source_timeline.data['label'] = tl_labels
        self._source_timeline.data['fill_color'] = tl_colors
