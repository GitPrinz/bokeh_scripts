# Phony will be executed no matter what.
.PHONY: doc
.DEFAULT_GOAL := help
###########################################################################################################
## SCRIPTS
###########################################################################################################

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
        match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
        if match:
                target, help = match.groups()
                print("%-20s %s" % (target, help))
endef

###########################################################################################################
## VARIABLES
###########################################################################################################

export PRINT_HELP_PYSCRIPT
ifeq ($(OS),Windows_NT)
    export PYTHON=py
else
    export PYTHON=python3
endif

###########################################################################################################
## ADD TARGETS FOR YOUR TASK
###########################################################################################################

help:
	@$(PYTHON) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

doc: ## create documentation
	cd doc && $(MAKE) html
	@echo "Open 'http://localhost:63342/bokeh_scripts/doc/_build/html/index.html' in a web browser"
	# not sure this link will work for all
	@echo "Or run 'make doc_view' to start a http.server"

doc_view: ## host documentation
	cd doc/_build/html && $(PYTHON) -m http.server

install:  ## Install this package
	$(MAKE) install-requirements
	$(MAKE) install-package

install-requirements:
	$(PYTHON) -m pip install -r requirements.txt

install-package:
	$(PYTHON) -m pip install -e .

test:  ## run scripts but doesn't start a bokeh server
	cd examples && $(PYTHON) main_map.py

demo:
	$(MAKE) demo_map

demo_map:  ## run the demo script with map
	cd examples && $(PYTHON) -m bokeh serve main_map.py

demo_simple:  ## run the demo script without map
	cd examples && $(PYTHON) -m bokeh serve main_simple.py

demo_timeline:  ## run the demo script without map (todo: will maybe fail because of paths)
	cd examples && $(PYTHON) example_timeline.py

clean:  ## delete every artifact
	cd doc && $(MAKE) clean