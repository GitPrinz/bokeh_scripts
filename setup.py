import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="bokeh_scripts",
    version="0.5.0",
    author="Martin Prinzler",
    author_email="git@martin-prinzler.de",
    description="Scripts for Logging and Debugging",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GitPrinz/bokeh_scripts",
    project_urls={
        "Bug Tracker": "https://gitlab.com/GitPrinz/bokeh_scripts/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha"
    ],
    packages=['bokeh_scripts'],
    python_requires=">=3.6",
    zip_safe=False
)
